#include "stdsimheader.h"
//#define BOOST_NO_STD_LOCALE
//#include <boost/format.hpp>

//#include "secondKernel.h"

namespace maxcompilersim {

void secondKernel::execute0() {
  { // Node ID: 51 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id52out_result;

  { // Node ID: 52 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id52in_a = id51out_io_output_force_disabled;

    id52out_result = (not_fixed(id52in_a));
  }
  { // Node ID: 82 (NodeConstantRawBits)
  }
  { // Node ID: 0 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 2 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id2in_enable = id82out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id2in_max = id0out_tokensIndex;

    HWOffsetFix<33,0,UNSIGNED> id2x_1;
    HWOffsetFix<1,0,UNSIGNED> id2x_2;
    HWOffsetFix<1,0,UNSIGNED> id2x_3;
    HWOffsetFix<33,0,UNSIGNED> id2x_4t_1e_1;

    id2out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id2st_count)));
    (id2x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id2st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id2x_2) = (gte_fixed((id2x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id2in_max))));
    (id2x_3) = (and_fixed((id2x_2),id2in_enable));
    id2out_wrap = (id2x_3);
    if((id2in_enable.getValueAsBool())) {
      if(((id2x_3).getValueAsBool())) {
        (id2st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id2x_4t_1e_1) = (id2x_1);
        (id2st_count) = (id2x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 81 (NodeConstantRawBits)
  }
  { // Node ID: 4 (NodeEq)
    const HWOffsetFix<32,0,UNSIGNED> &id4in_a = id2out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id4in_b = id81out_value;

    id4out_result[(getCycle()+1)%2] = (eq_fixed(id4in_a,id4in_b));
  }
  { // Node ID: 5 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id6out_result;

  { // Node ID: 6 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id6in_a = id5out_io_px_force_disabled;

    id6out_result = (not_fixed(id6in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id7out_result;

  { // Node ID: 7 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id7in_a = id4out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id7in_b = id6out_result;

    HWOffsetFix<1,0,UNSIGNED> id7x_1;

    (id7x_1) = (and_fixed(id7in_a,id7in_b));
    id7out_result = (id7x_1);
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 8 (NodeInput)
    const HWOffsetFix<1,0,UNSIGNED> &id8in_enable = id7out_result;

    (id8st_read_next_cycle) = ((id8in_enable.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    queueReadRequest(m_px, id8st_read_next_cycle.getValueAsBool());
  }
  HWOffsetFix<10,0,UNSIGNED> id29out_o;

  { // Node ID: 29 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id29in_i = id2out_count;

    id29out_o = (cast_fixed2fixed<10,0,UNSIGNED,TONEAR>(id29in_i));
  }
  { // Node ID: 70 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id70in_input = id29out_o;

    id70out_output[(getCycle()+4)%5] = id70in_input;
  }
  { // Node ID: 57 (NodeMappedRom)
    const HWOffsetFix<10,0,UNSIGNED> &id57in_addra = id70out_output[getCycle()%5];

    long id57x_1;
    HWFloat<8,24> id57x_2;

    (id57x_1) = (id57in_addra.getValueAsLong());
    switch(((long)((id57x_1)<(1000l)))) {
      case 0l:
        id57x_2 = (c_hw_flt_8_24_undef);
        break;
      case 1l:
        id57x_2 = (getMappedMemValue< HWFloat<8,24> > ("mappedRomRho", id57x_1) );
        break;
      default:
        id57x_2 = (c_hw_flt_8_24_undef);
        break;
    }
    id57out_dataa[(getCycle()+2)%3] = (id57x_2);
  }
  { // Node ID: 32 (NodeMul)
    const HWFloat<8,24> &id32in_a = id8out_data;
    const HWFloat<8,24> &id32in_b = id57out_dataa[getCycle()%3];

    id32out_result[(getCycle()+8)%9] = (mul_float(id32in_a,id32in_b));
  }
  { // Node ID: 71 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id71in_input = id4out_result[getCycle()%2];

    id71out_output[(getCycle()+8)%9] = id71in_input;
  }
  { // Node ID: 9 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id10out_result;

  { // Node ID: 10 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id10in_a = id9out_io_bx_force_disabled;

    id10out_result = (not_fixed(id10in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id11out_result;

  { // Node ID: 11 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id11in_a = id71out_output[getCycle()%9];
    const HWOffsetFix<1,0,UNSIGNED> &id11in_b = id10out_result;

    HWOffsetFix<1,0,UNSIGNED> id11x_1;

    (id11x_1) = (and_fixed(id11in_a,id11in_b));
    id11out_result = (id11x_1);
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 12 (NodeInput)
    const HWOffsetFix<1,0,UNSIGNED> &id12in_enable = id11out_result;

    (id12st_read_next_cycle) = ((id12in_enable.getValueAsBool())&(!(((getFlushLevel())>=(12l))&(isFlushingActive()))));
    queueReadRequest(m_bx, id12st_read_next_cycle.getValueAsBool());
  }
  { // Node ID: 33 (NodeAdd)
    const HWFloat<8,24> &id33in_a = id32out_result[getCycle()%9];
    const HWFloat<8,24> &id33in_b = id12out_data;

    id33out_result[(getCycle()+12)%13] = (add_float(id33in_a,id33in_b));
  }
  { // Node ID: 77 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id77in_input = id70out_output[getCycle()%5];

    id77out_output[(getCycle()+14)%15] = id77in_input;
  }
  { // Node ID: 55 (NodeMappedRom)
    const HWOffsetFix<10,0,UNSIGNED> &id55in_addra = id77out_output[getCycle()%15];

    long id55x_1;
    HWOffsetFix<32,0,TWOSCOMPLEMENT> id55x_2;

    (id55x_1) = (id55in_addra.getValueAsLong());
    switch(((long)((id55x_1)<(1000l)))) {
      case 0l:
        id55x_2 = (c_hw_fix_32_0_sgn_undef);
        break;
      case 1l:
        id55x_2 = (getMappedMemValue< HWOffsetFix<32,0,TWOSCOMPLEMENT> > ("mappedRomX", id55x_1) );
        break;
      default:
        id55x_2 = (c_hw_fix_32_0_sgn_undef);
        break;
    }
    id55out_dataa[(getCycle()+2)%3] = (id55x_2);
  }
  { // Node ID: 30 (NodeCast)
    const HWOffsetFix<32,0,TWOSCOMPLEMENT> &id30in_i = id55out_dataa[getCycle()%3];

    id30out_o[(getCycle()+6)%7] = (cast_fixed2float<8,24>(id30in_i));
  }
  { // Node ID: 34 (NodeSub)
    const HWFloat<8,24> &id34in_a = id33out_result[getCycle()%13];
    const HWFloat<8,24> &id34in_b = id30out_o[getCycle()%7];

    id34out_result[(getCycle()+12)%13] = (sub_float(id34in_a,id34in_b));
  }
  { // Node ID: 38 (NodeMul)
    const HWFloat<8,24> &id38in_a = id34out_result[getCycle()%13];
    const HWFloat<8,24> &id38in_b = id34out_result[getCycle()%13];

    id38out_result[(getCycle()+8)%9] = (mul_float(id38in_a,id38in_b));
  }
  { // Node ID: 13 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id14out_result;

  { // Node ID: 14 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id14in_a = id13out_io_py_force_disabled;

    id14out_result = (not_fixed(id14in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id15out_result;

  { // Node ID: 15 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id15in_a = id4out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id15in_b = id14out_result;

    HWOffsetFix<1,0,UNSIGNED> id15x_1;

    (id15x_1) = (and_fixed(id15in_a,id15in_b));
    id15out_result = (id15x_1);
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 16 (NodeInput)
    const HWOffsetFix<1,0,UNSIGNED> &id16in_enable = id15out_result;

    (id16st_read_next_cycle) = ((id16in_enable.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    queueReadRequest(m_py, id16st_read_next_cycle.getValueAsBool());
  }
  { // Node ID: 35 (NodeMul)
    const HWFloat<8,24> &id35in_a = id16out_data;
    const HWFloat<8,24> &id35in_b = id57out_dataa[getCycle()%3];

    id35out_result[(getCycle()+8)%9] = (mul_float(id35in_a,id35in_b));
  }
  { // Node ID: 17 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id18out_result;

  { // Node ID: 18 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id18in_a = id17out_io_by_force_disabled;

    id18out_result = (not_fixed(id18in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id19out_result;

  { // Node ID: 19 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id19in_a = id71out_output[getCycle()%9];
    const HWOffsetFix<1,0,UNSIGNED> &id19in_b = id18out_result;

    HWOffsetFix<1,0,UNSIGNED> id19x_1;

    (id19x_1) = (and_fixed(id19in_a,id19in_b));
    id19out_result = (id19x_1);
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 20 (NodeInput)
    const HWOffsetFix<1,0,UNSIGNED> &id20in_enable = id19out_result;

    (id20st_read_next_cycle) = ((id20in_enable.getValueAsBool())&(!(((getFlushLevel())>=(12l))&(isFlushingActive()))));
    queueReadRequest(m_by, id20st_read_next_cycle.getValueAsBool());
  }
  { // Node ID: 36 (NodeAdd)
    const HWFloat<8,24> &id36in_a = id35out_result[getCycle()%9];
    const HWFloat<8,24> &id36in_b = id20out_data;

    id36out_result[(getCycle()+12)%13] = (add_float(id36in_a,id36in_b));
  }
  { // Node ID: 56 (NodeMappedRom)
    const HWOffsetFix<10,0,UNSIGNED> &id56in_addra = id77out_output[getCycle()%15];

    long id56x_1;
    HWOffsetFix<32,0,TWOSCOMPLEMENT> id56x_2;

    (id56x_1) = (id56in_addra.getValueAsLong());
    switch(((long)((id56x_1)<(1000l)))) {
      case 0l:
        id56x_2 = (c_hw_fix_32_0_sgn_undef);
        break;
      case 1l:
        id56x_2 = (getMappedMemValue< HWOffsetFix<32,0,TWOSCOMPLEMENT> > ("mappedRomY", id56x_1) );
        break;
      default:
        id56x_2 = (c_hw_fix_32_0_sgn_undef);
        break;
    }
    id56out_dataa[(getCycle()+2)%3] = (id56x_2);
  }
  { // Node ID: 31 (NodeCast)
    const HWOffsetFix<32,0,TWOSCOMPLEMENT> &id31in_i = id56out_dataa[getCycle()%3];

    id31out_o[(getCycle()+6)%7] = (cast_fixed2float<8,24>(id31in_i));
  }
  { // Node ID: 37 (NodeSub)
    const HWFloat<8,24> &id37in_a = id36out_result[getCycle()%13];
    const HWFloat<8,24> &id37in_b = id31out_o[getCycle()%7];

    id37out_result[(getCycle()+12)%13] = (sub_float(id37in_a,id37in_b));
  }
  { // Node ID: 39 (NodeMul)
    const HWFloat<8,24> &id39in_a = id37out_result[getCycle()%13];
    const HWFloat<8,24> &id39in_b = id37out_result[getCycle()%13];

    id39out_result[(getCycle()+8)%9] = (mul_float(id39in_a,id39in_b));
  }
  { // Node ID: 40 (NodeAdd)
    const HWFloat<8,24> &id40in_a = id38out_result[getCycle()%9];
    const HWFloat<8,24> &id40in_b = id39out_result[getCycle()%9];

    id40out_result[(getCycle()+12)%13] = (add_float(id40in_a,id40in_b));
  }
  { // Node ID: 80 (NodeConstantRawBits)
  }
  { // Node ID: 42 (NodeLt)
    const HWFloat<8,24> &id42in_a = id40out_result[getCycle()%13];
    const HWFloat<8,24> &id42in_b = id80out_value;

    id42out_result[(getCycle()+2)%3] = (lt_float(id42in_a,id42in_b));
  }
  { // Node ID: 21 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id22out_result;

  { // Node ID: 22 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id22in_a = id21out_io_siRho_force_disabled;

    id22out_result = (not_fixed(id22in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id23out_result;

  { // Node ID: 23 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id23in_a = id4out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id23in_b = id22out_result;

    HWOffsetFix<1,0,UNSIGNED> id23x_1;

    (id23x_1) = (and_fixed(id23in_a,id23in_b));
    id23out_result = (id23x_1);
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 24 (NodeInput)
    const HWOffsetFix<1,0,UNSIGNED> &id24in_enable = id23out_result;

    (id24st_read_next_cycle) = ((id24in_enable.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    queueReadRequest(m_siRho, id24st_read_next_cycle.getValueAsBool());
  }
  { // Node ID: 43 (NodeLt)
    const HWFloat<8,24> &id43in_a = id57out_dataa[getCycle()%3];
    const HWFloat<8,24> &id43in_b = id24out_data;

    id43out_result[(getCycle()+2)%3] = (lt_float(id43in_a,id43in_b));
  }
  { // Node ID: 75 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id75in_input = id43out_result[getCycle()%3];

    id75out_output[(getCycle()+52)%53] = id75in_input;
  }
  { // Node ID: 44 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id44in_a = id42out_result[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id44in_b = id75out_output[getCycle()%53];

    HWOffsetFix<1,0,UNSIGNED> id44x_1;

    (id44x_1) = (and_fixed(id44in_a,id44in_b));
    id44out_result[(getCycle()+1)%2] = (id44x_1);
  }
  { // Node ID: 25 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id26out_result;

  { // Node ID: 26 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id26in_a = id25out_io_soRho_force_disabled;

    id26out_result = (not_fixed(id26in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id27out_result;

  { // Node ID: 27 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id27in_a = id4out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id27in_b = id26out_result;

    HWOffsetFix<1,0,UNSIGNED> id27x_1;

    (id27x_1) = (and_fixed(id27in_a,id27in_b));
    id27out_result = (id27x_1);
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 28 (NodeInput)
    const HWOffsetFix<1,0,UNSIGNED> &id28in_enable = id27out_result;

    (id28st_read_next_cycle) = ((id28in_enable.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    queueReadRequest(m_soRho, id28st_read_next_cycle.getValueAsBool());
  }
  { // Node ID: 45 (NodeGt)
    const HWFloat<8,24> &id45in_a = id57out_dataa[getCycle()%3];
    const HWFloat<8,24> &id45in_b = id28out_data;

    id45out_result[(getCycle()+2)%3] = (gt_float(id45in_a,id45in_b));
  }
  { // Node ID: 76 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id76in_input = id45out_result[getCycle()%3];

    id76out_output[(getCycle()+53)%54] = id76in_input;
  }
  { // Node ID: 46 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id46in_a = id44out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id46in_b = id76out_output[getCycle()%54];

    HWOffsetFix<1,0,UNSIGNED> id46x_1;

    (id46x_1) = (and_fixed(id46in_a,id46in_b));
    id46out_result[(getCycle()+1)%2] = (id46x_1);
  }
  { // Node ID: 48 (NodeConstantRawBits)
  }
  { // Node ID: 47 (NodeConstantRawBits)
  }
  { // Node ID: 49 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id49in_sel = id46out_result[getCycle()%2];
    const HWOffsetFix<16,0,UNSIGNED> &id49in_option0 = id48out_value;
    const HWOffsetFix<16,0,UNSIGNED> &id49in_option1 = id47out_value;

    HWOffsetFix<16,0,UNSIGNED> id49x_1;

    switch((id49in_sel.getValueAsLong())) {
      case 0l:
        id49x_1 = id49in_option0;
        break;
      case 1l:
        id49x_1 = id49in_option1;
        break;
      default:
        id49x_1 = (c_hw_fix_16_0_uns_undef);
        break;
    }
    id49out_result[(getCycle()+1)%2] = (id49x_1);
  }
  if ( (getFillLevel() >= (66l)) && (getFlushLevel() < (66l)|| !isFlushingActive() ))
  { // Node ID: 54 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id54in_output_control = id52out_result;
    const HWOffsetFix<16,0,UNSIGNED> &id54in_data = id49out_result[getCycle()%2];

    bool id54x_1;

    (id54x_1) = ((id54in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(66l))&(isFlushingActive()))));
    if((id54x_1)) {
      writeOutput(m_output, id54in_data);
    }
  }
  { // Node ID: 62 (NodeConstantRawBits)
  }
  { // Node ID: 79 (NodeConstantRawBits)
  }
  { // Node ID: 59 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 60 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id60in_enable = id79out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id60in_max = id59out_value;

    HWOffsetFix<49,0,UNSIGNED> id60x_1;
    HWOffsetFix<1,0,UNSIGNED> id60x_2;
    HWOffsetFix<1,0,UNSIGNED> id60x_3;
    HWOffsetFix<49,0,UNSIGNED> id60x_4t_1e_1;

    id60out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id60st_count)));
    (id60x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id60st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id60x_2) = (gte_fixed((id60x_1),id60in_max));
    (id60x_3) = (and_fixed((id60x_2),id60in_enable));
    id60out_wrap = (id60x_3);
    if((id60in_enable.getValueAsBool())) {
      if(((id60x_3).getValueAsBool())) {
        (id60st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id60x_4t_1e_1) = (id60x_1);
        (id60st_count) = (id60x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id61out_output;

  { // Node ID: 61 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id61in_input = id60out_count;

    id61out_output = id61in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 63 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id63in_load = id62out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id63in_data = id61out_output;

    bool id63x_1;

    (id63x_1) = ((id63in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id63x_1)) {
      setMappedRegValue("current_run_cycle_count", id63in_data);
    }
  }
  { // Node ID: 78 (NodeConstantRawBits)
  }
  { // Node ID: 65 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 66 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id66in_enable = id78out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id66in_max = id65out_value;

    HWOffsetFix<49,0,UNSIGNED> id66x_1;
    HWOffsetFix<1,0,UNSIGNED> id66x_2;
    HWOffsetFix<1,0,UNSIGNED> id66x_3;
    HWOffsetFix<49,0,UNSIGNED> id66x_4t_1e_1;

    id66out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id66st_count)));
    (id66x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id66st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id66x_2) = (gte_fixed((id66x_1),id66in_max));
    (id66x_3) = (and_fixed((id66x_2),id66in_enable));
    id66out_wrap = (id66x_3);
    if((id66in_enable.getValueAsBool())) {
      if(((id66x_3).getValueAsBool())) {
        (id66st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id66x_4t_1e_1) = (id66x_1);
        (id66st_count) = (id66x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 68 (NodeInputMappedReg)
  }
  { // Node ID: 69 (NodeEq)
    const HWOffsetFix<48,0,UNSIGNED> &id69in_a = id66out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id69in_b = id68out_run_cycle_count;

    id69out_result[(getCycle()+1)%2] = (eq_fixed(id69in_a,id69in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 67 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id67in_start = id69out_result[getCycle()%2];

    if((id67in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
