#ifndef BWTTKERNEL_H_
#define BWTTKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class bwttKernel : public KernelManagerBlockSync {
public:
  bwttKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_so;
  t_port_number m_si;
  t_port_number m_px;
  t_port_number m_bx;
  t_port_number m_py;
  t_port_number m_by;
  t_port_number m_siRho;
  t_port_number m_soRho;
  HWOffsetFix<1,0,UNSIGNED> id23out_io_px_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3out_io_so_force_disabled;

  HWOffsetFix<32,0,TWOSCOMPLEMENT> id5out_data;

  HWOffsetFix<1,0,UNSIGNED> id5st_read_next_cycle;
  HWOffsetFix<32,0,TWOSCOMPLEMENT> id5st_last_read_value;

  HWOffsetFix<1,0,UNSIGNED> id0out_io_si_force_disabled;

  HWOffsetFix<32,0,TWOSCOMPLEMENT> id2out_data;

  HWOffsetFix<1,0,UNSIGNED> id2st_read_next_cycle;
  HWOffsetFix<32,0,TWOSCOMPLEMENT> id2st_last_read_value;

  HWOffsetFix<32,0,TWOSCOMPLEMENT> id52out_dataa[3];
  HWOffsetFix<32,0,TWOSCOMPLEMENT> id52out_datab[3];

  HWFloat<8,24> id10out_o[7];

  HWFloat<8,24> id7out_o[7];

  HWFloat<8,24> id12out_result[13];

  HWOffsetFix<10,0,UNSIGNED> id67out_output[7];

  HWOffsetFix<10,0,UNSIGNED> id68out_output[7];

  HWFloat<8,24> id54out_dataa[3];
  HWFloat<8,24> id54out_datab[3];

  HWFloat<8,24> id13out_result[13];

  HWFloat<8,24> id14out_result[29];

  HWOffsetFix<1,0,UNSIGNED> id28out_io_bx_force_disabled;

  HWFloat<8,24> id70out_output[49];

  HWFloat<8,24> id69out_output[41];

  HWFloat<8,24> id15out_result[9];

  HWFloat<8,24> id16out_result[13];

  HWOffsetFix<1,0,UNSIGNED> id33out_io_py_force_disabled;

  HWOffsetFix<32,0,TWOSCOMPLEMENT> id53out_dataa[3];
  HWOffsetFix<32,0,TWOSCOMPLEMENT> id53out_datab[3];

  HWFloat<8,24> id11out_o[7];

  HWFloat<8,24> id8out_o[7];

  HWFloat<8,24> id17out_result[13];

  HWFloat<8,24> id19out_result[29];

  HWOffsetFix<1,0,UNSIGNED> id38out_io_by_force_disabled;

  HWFloat<8,24> id72out_output[49];

  HWFloat<8,24> id20out_result[9];

  HWFloat<8,24> id21out_result[13];

  HWOffsetFix<1,0,UNSIGNED> id43out_io_siRho_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id48out_io_soRho_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id59out_value;

  HWOffsetFix<1,0,UNSIGNED> id75out_value;

  HWOffsetFix<49,0,UNSIGNED> id56out_value;

  HWOffsetFix<48,0,UNSIGNED> id57out_count;
  HWOffsetFix<1,0,UNSIGNED> id57out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id57st_count;

  HWOffsetFix<1,0,UNSIGNED> id74out_value;

  HWOffsetFix<49,0,UNSIGNED> id62out_value;

  HWOffsetFix<48,0,UNSIGNED> id63out_count;
  HWOffsetFix<1,0,UNSIGNED> id63out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id63st_count;

  HWOffsetFix<48,0,UNSIGNED> id65out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id66out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<32,0,TWOSCOMPLEMENT> c_hw_fix_32_0_sgn_undef;
  const HWOffsetFix<10,0,UNSIGNED> c_hw_fix_10_0_uns_undef;
  const HWFloat<8,24> c_hw_flt_8_24_undef;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* BWTTKERNEL_H_ */
