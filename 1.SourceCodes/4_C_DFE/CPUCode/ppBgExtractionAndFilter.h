/**
* @file ppBgExtractionAndFilter.h
* @Funtions for Processing Grey Image of Whiskers
* @author Yang Ma, Erasmus MC
*/

/*
% This function attempts to find the snout contour using a pre-defined contour template
% Inputs:
%           -(snoutCenterX, snoutCenterY) is the user selected snout center
%           -(noseTipX, noseTipY) is the user selected nose tip
%           -sdContour is a vector of points extracted from previous section
% Outputs:
%           -snoutContour is the estiamted contour of snout
*/

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>


void ppExtractionAndFilter(IplImage *greyFrame, int pixs, uchar *ppBg);
void imadjust(uchar *frameData, int pixs, int lowIn, int highIn, double gamma);

void ppExtractionAndFilter(IplImage *greyFrame, int pixs, uchar *ppBg) {

	// Parameter Setting
	double bwThreshold = 0.2;
	double histEqGamma = 2.5;
	int histEqMinT = 0;
	int histEqMaxT = 64;
	int razorSize = 15;

	/*
		uchar razorDisk[1000] = {0};
	createDiskStrel(razorSize, razorDisk);
	IplConvKernel *erodeDisk = cvCreateStructuringElementEx(razorSize * 2 - 1,
		razorSize * 2 - 1, 0, 0, CV_SHAPE_CUSTOM, razorDisk);
	*/

	IplConvKernel *erodeDisk = cvCreateStructuringElementEx(razorSize * 2 - 1,
		razorSize * 2 - 1, 0, 0, CV_SHAPE_ELLIPSE, NULL);

	IplImage *bwShape = cvCloneImage(greyFrame);
	stExtractEnvGrayValuesWithMorphology(bwShape, bwThreshold, erodeDisk);

	uchar *frameData;
	frameData = (uchar *)greyFrame->imageData;

	uchar *bwShapeData;
	bwShapeData = (uchar *)bwShape->imageData;

	// Background Extraction
	for (int i = 0; i < pixs; i++) {
		frameData[i] = (ppBg[i] - frameData[i])*bwShapeData[i];
	}

	imadjust(frameData, pixs, histEqMinT, histEqMaxT, histEqGamma);



}


void imadjust(uchar *frameData, int pixs, int lowIn, int highIn, double gamma)
{
	// src : input CV_8UC1 image
	// highIn  : src image bounds

	for (int i = 0; i < pixs; i++) {
		if (frameData[i] > highIn) {
			frameData[i] = highIn;
		}
		double temp = 255 * pow(((double)frameData[i] / highIn), gamma) + 0.5;
		frameData[i] = (int)temp;
	}

}
