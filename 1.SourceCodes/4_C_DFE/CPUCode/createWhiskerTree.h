/**
* @file wdIgorMeanAngle.h
* @Funtions for Extracting Whisker Segments
* @author Yang Ma, Erasmus MC
*/

/*
% This function attempts to find the snout contour using a pre-defined contour template
% Inputs:
%           -(snoutCenterX, snoutCenterY) is the user selected snout center
%           -(noseTipX, noseTipY) is the user selected nose tip
%           -sdContour is a vector of points extracted from previous section
% Outputs:
%           -snoutContour is the estiamted contour of snout
*/


#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

struct WhiskerTree {
	int X;
	int Y;
	float intensity;
	float rho;
	float theta;
};

struct WhiskerTree whiskerTree[20000];
struct WhiskerTree selectedWTree[1000];
int tokensIndex = 0;

void createWhiskerTree(IplImage *duoImage, int potentialDensityArray, int snoutCenterX,
	int snoutCenterY, float trBinary, int cols, int rows);
void QuickSortTree(int left, int right);
int findLocalTokens(int localTokensN,
	float trInnerRing, int start, int end, int rhoRange, int *idx);
void groupConnectedTokens(int *idx, int tokenCount);

void createWhiskerTree(IplImage *duoImage, int potentialDensityArray, int snoutCenterX,
	int snoutCenterY, float trBinary, int cols, int rows) {

	int minimumRho = 30;
	int rhoThresh = 1000;
	int localTokensN = 6;
	int peakT = 40;
	float trInnerRing = peakT/255.0f;
	tokensIndex = 0;

	// Transfer to double format
	IplImage *duoFloatImage = cvCreateImage(
		cvSize(cols, rows), IPL_DEPTH_32F, 1);
	float *newDuoData = (float *)duoFloatImage->imageData;;
	uchar *duoData = (uchar *)duoImage->imageData;
	float tempMax = 0;

	for (int i = 0; i < cols*rows; i++) {
		newDuoData[i] = duoData[i];
		if (duoData[i] > tempMax)
			tempMax = duoData[i];
	}

	for (int i = 0; i < cols*rows; i++) {
		newDuoData[i] = newDuoData[i]/tempMax;
	}


	// Initialize Conv kernel
	float data[9];
	for (int i = 0; i < 9; i++) {
		data[i] = 1 / 9.0f;
	}
	CvMat mat;
	cvInitMatHeader(&mat, 3, 3, CV_32FC1, data, CV_AUTOSTEP);

	/*
	float *matdata = mat.data.fl;
	for (int i = 0; i < 9; i++) {
	fprintf(file, "%.2f\n", matdata[i]);
	}
	*/

	cvFilter2D(duoFloatImage, duoFloatImage, &mat, cvPoint(-1, -1));
	IplImage *biImage = cvCloneImage(duoFloatImage);
	cvThreshold(biImage, biImage, trBinary, 1, CV_THRESH_BINARY);



	// Calcualte rhos
	//uchar *duoData = (uchar *)duoImage->imageData;
	float *biData = (float *)biImage->imageData;
	int wtCount = 0;
	int maxRho = 0;
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			if (biData[i*cols + j] == 1) {
				double tempX = j - snoutCenterX;
				double tempY = i - snoutCenterY;
				float tempRho = sqrt(tempX*tempX + tempY*tempY);
				if (tempRho > maxRho) {
					maxRho = (int)tempRho;
				}
				whiskerTree[wtCount].rho = tempRho;
				whiskerTree[wtCount].theta = atan2(tempY, tempX);
				whiskerTree[wtCount].X = j;
				whiskerTree[wtCount].Y = i;
				whiskerTree[wtCount].intensity = newDuoData[i*cols + j];
				wtCount++;
			}
		}
	}

	//Sort According to Rho
	QuickSortTree(0, wtCount - 1);

	int wtStart = 0;
	// Search through all radis
	for (int i = 0; i < maxRho; i++) {
		int prev = wtStart;
		for (int j = wtStart; j < wtCount; j++) {
			//Rule Out Facial Hair
			if (fabs(whiskerTree[j].rho - (float)(minimumRho + i)) < 1) {
				//struct WhiskerTree temp = whiskerTree[wtStart];
				whiskerTree[wtStart++] = whiskerTree[j];
			}
		}
		if (wtStart > prev) {

			int idx[1000];
			int tokenCount = findLocalTokens(localTokensN,
				trInnerRing, prev, wtStart, i+minimumRho, idx);

			if (tokenCount > 0) {
				// Bubble sort by theta (convinent for small array)
				for (int i = 0; i < tokenCount; i++) {
					for (int j = i + 1; j < tokenCount; j++) {
						if (whiskerTree[idx[j]].theta < whiskerTree[idx[i]].theta) {
							int temp = idx[i];
							idx[i] = idx[j];
							idx[j] = temp;
						}
					}
				}
				groupConnectedTokens(idx, tokenCount);
			}
		}
	}

	/*
	FILE *file = fopen("filename", "w");
	for (int i = 0; i < tokensIndex; i++) {
	fprintf(file, "%d\n", selectedWTree[i].X);
	fprintf(file, "%d\n", selectedWTree[i].Y);
	}
	fclose(file);
	*/


	//Sort by rho, theta



	cvReleaseImage(&biImage);
	cvReleaseImage(&duoFloatImage);

}

void QuickSortTree(int left, int right) {

	int i = left, j = right;
	float key = whiskerTree[left].rho;
	struct WhiskerTree temp = whiskerTree[left];

	if (left >= right) {
		return;
	}

	while (i < j) {

		while (i<j && whiskerTree[j].rho > key)
			j--;
		whiskerTree[i++] = whiskerTree[j];
		while (i<j && whiskerTree[i].rho < key)
			i++;
		if (i < j) {
			whiskerTree[j--] = whiskerTree[i];
		}
	}

	whiskerTree[i] = temp;
	QuickSortTree(left, i - 1);
	QuickSortTree(i + 1, right);
}

int findLocalTokens(int localTokensN,
	float trInnerRing, int start, int end, int rhoRange, int *idx) {

	int rhoThresh = 1000;
	int trOuterRing = 0;
	float Thresh;

	if (rhoRange < rhoThresh)
		Thresh = trInnerRing;
	else
		Thresh = trOuterRing;

	float maxIntens = 0;
	int maxCount = 0;

	//Neighboring case
	if (end - start > localTokensN / 2) {
		int L = end - start;
		float f[1000], maxVec[1000], rightVec[1000], leftVec[1000];
		for (int i = start; i < end; i++) {
			f[i - start] = whiskerTree[i].intensity;
			maxVec[i - start] = f[i - start];
		}

		for (int i = 0; i < localTokensN / 2; i++) {
			for (int j = 0; j < L; j++) {
				if (j <= i) {
					rightVec[j] = 0;
				}
				else {
					rightVec[j] = f[j - i - 1];
				}

				if (j >= L - i - 1) {
					leftVec[j] = 0;
				}
				else {
					leftVec[j] = f[i + j + 1];
				}
				float tempVec = rightVec[j]>leftVec[j]?rightVec[j]:leftVec[j];
				if(maxVec[j] < tempVec){
					maxVec[j] = tempVec;
				}
				//maxVec[j] = max(maxVec[j], max(rightVec[j], leftVec[j]));
			}
		}

		for (int i = start; i < end; i++) {
			if (whiskerTree[i].intensity == maxVec[i-start] &&
				maxVec[i - start] > Thresh) {
				idx[maxCount++] = i;
			}
		}
	}
	else { // Simple case
		for (int i = start; i < end; i++) {
			if (whiskerTree[i].intensity > maxIntens) {
				maxIntens = whiskerTree[i].intensity;
			}
		}

		if (maxIntens <= Thresh) {
			return -1;
		}

		for (int i = start; i < end; i++) {
			if (whiskerTree[i].intensity == maxIntens) {
				idx[maxCount++] = i;
			}
		}
	}
	return maxCount;

}

void groupConnectedTokens(int *idx, int tokenCount) {

	int currentX = 0, currentY = 0;
	int xOfToken[50], yOfToken[50];
	float thetaOfToken[50], rhoOfToken[50], iOfToken[50];
	int tempCount = 0;

	for (int i = 0; i < tokenCount; i++) {
		int X = whiskerTree[idx[i]].X;
		int Y = whiskerTree[idx[i]].Y;
		if (abs(currentX - X) > 1 || abs(currentY - Y) > 1) {
			if (i > 0) {
				int averageX = 0, averageY = 0;
				float averageTheta = 0, averageRho = 0, averageI = 0;
				if (tempCount > 2) {
					int maxXofToken = 0, maxYofToken = 0;
					int minXofToken = INT_MAX, minYofToken = INT_MAX;
					for (int i = 0; i < tempCount; i++) {
						if (maxXofToken < xOfToken[i])
							maxXofToken = xOfToken[i];
						if (maxYofToken < yOfToken[i])
							maxYofToken = yOfToken[i];
						if (minXofToken > xOfToken[i])
							minXofToken = xOfToken[i];
						if (minYofToken < yOfToken[i])
							minYofToken = yOfToken[i];
					}
					if (maxXofToken - minXofToken + 1 == tempCount || maxYofToken - minYofToken + 1 == tempCount) {
						selectedWTree[tokensIndex].X = xOfToken[0];
						selectedWTree[tokensIndex].Y = yOfToken[0];
						selectedWTree[tokensIndex].theta = thetaOfToken[0];
						selectedWTree[tokensIndex].intensity = iOfToken[0];
						selectedWTree[tokensIndex++].rho = rhoOfToken[0];
						selectedWTree[tokensIndex].X = xOfToken[tempCount - 1];
						selectedWTree[tokensIndex].Y = yOfToken[tempCount - 1];
						selectedWTree[tokensIndex].theta = thetaOfToken[tempCount - 1];
						selectedWTree[tokensIndex].intensity = iOfToken[tempCount - 1];
						selectedWTree[tokensIndex++].rho = rhoOfToken[tempCount - 1];
					}
					else {
						for (int i = 0; i < tempCount; i++) {
							averageX += xOfToken[i];
							averageY += yOfToken[i];
							averageRho += rhoOfToken[i];
							averageTheta += thetaOfToken[i];
							averageI += iOfToken[i];
						}

						selectedWTree[tokensIndex].X = averageX / tempCount;
						selectedWTree[tokensIndex].Y = averageY / tempCount;
						selectedWTree[tokensIndex].theta = averageTheta / tempCount;
						selectedWTree[tokensIndex].intensity = averageI / tempCount;
						selectedWTree[tokensIndex++].rho = averageRho / tempCount;
					}
				}
				else {

					// Calculate average;
					for (int i = 0; i < tempCount; i++) {
						averageX += xOfToken[i];
						averageY += yOfToken[i];
						averageRho += rhoOfToken[i];
						averageTheta += thetaOfToken[i];
						averageI += iOfToken[i];
					}
					selectedWTree[tokensIndex].X = averageX / tempCount;
					selectedWTree[tokensIndex].Y = averageY / tempCount;
					selectedWTree[tokensIndex].theta = averageTheta / tempCount;
					selectedWTree[tokensIndex].intensity = averageI / tempCount;
					selectedWTree[tokensIndex++].rho = averageRho / tempCount;
				}
				tempCount = 0;
			}
		}
		xOfToken[tempCount] = X;
		yOfToken[tempCount] = Y;
		iOfToken[tempCount] = whiskerTree[idx[i]].intensity;
		rhoOfToken[tempCount] = whiskerTree[idx[i]].rho;
		thetaOfToken[tempCount] = whiskerTree[idx[i]].theta;
		currentX = X;
		currentY = Y;
		tempCount++;
	}

	if (tempCount > 0) {
		int averageX = 0, averageY = 0;
		float averageTheta = 0, averageRho = 0, averageI = 0;
		// Calculate average;
		for (int i = 0; i < tempCount; i++) {
			averageX += xOfToken[i];
			averageY += yOfToken[i];
			averageRho += rhoOfToken[i];
			averageTheta += thetaOfToken[i];
			averageI += iOfToken[i];
		}
		selectedWTree[tokensIndex].X = averageX / tempCount;
		selectedWTree[tokensIndex].Y = averageY / tempCount;
		selectedWTree[tokensIndex].theta = averageTheta / tempCount;
		selectedWTree[tokensIndex].intensity = averageI / tempCount;
		selectedWTree[tokensIndex++].rho = averageRho / tempCount;
	}
}
