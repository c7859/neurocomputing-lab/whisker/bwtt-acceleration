#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <omp.h>
#include "sdGeneric.h"
#include "stShapeSpaceKalman.h"
#include "ppBgExtractionAndFilter.h"
#include "wdIgorMeanAngle.h"



int main(void)
{

	//-----------------------Main Parameter Setting--------------------
	// Segment of frame for processing
	int startFrame = 8000, endFrame = 10000;

	// Parameters
	int snoutCenterX = 389;
	int snoutCenterY = 145;
	int snoutTipX = 417;
	int snoutTipY = 328;

	double bwThreshold = 0.2;
	int razorSize = 15;

	FILE * fp;
	fp = fopen("parameters", "r");

	fscanf(fp, "Starting Frame Index:%d\n", &startFrame);
	fscanf(fp, "End Frame Index:%d\n", &endFrame);
	fscanf(fp, "Snout Center X:%d\n", &snoutCenterX);
	fscanf(fp, "Snout Center Y:%d\n", &snoutCenterY);
	fscanf(fp, "Snout Tip X:%d\n", &snoutTipX);
	fscanf(fp, "Snout Tip Y:%d", &snoutTipY);
	fclose(fp);

	/*
	printf("Please enter the index of starting frame: \n");
	scanf("%d", &startFrame);
	printf("Please enter the index of end frame: \n");
	scanf("%d", &endFrame);
	printf("Please enter the X of snout center: \n");
	scanf("%d", &snoutCenterX);
	printf("Please enter the Y of snout center: \n");
	scanf("%d", &snoutCenterY);
	printf("Please enter the X of snout tip: \n");
	scanf("%d", &snoutTipX);
	printf("Please enter the Y of snout tip \n");
	scanf("%d", &snoutTipY);
	*/


	// Read Video File
	CvCapture* capture = cvCaptureFromFile("150924-132427-003-00000-00000.avi");
	//--------------------------The End ---------------------------------


	uchar *frameData;
	IplImage *frame = 0;
	int boundaryX[10000];
	int boundaryY[10000];

	FILE *file1;
	file1 = fopen("source", "w");



	//cvNamedWindow("Capture", CV_WINDOW_AUTOSIZE);

	//cvShowImage("Capture", frame); // Display the frame

	/*
	for (int i = 0; i < nRows; i++)
			for (int j = 0; j < nCols; j++)
				if (frameData[i*nCols + j] > ppBg[i*nCols + j]) {
					ppBg[i*nCols + j] = frameData[i*nCols + j];
				}
	*/

	int frameNumbers = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_COUNT);

	if (endFrame > frameNumbers) {
		endFrame = frameNumbers;
	}

	int nRows = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_HEIGHT);
	int nCols = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_WIDTH);
	//int step = 2;
	int step = (frameNumbers - startFrame) / 1000;
	int pixs = nRows*nCols;

	IplImage *greyframe = cvCreateImage(
		cvSize(nCols, nRows), IPL_DEPTH_8U, 1);



	uchar *ppBg;

	ppBg = (uchar*)calloc(nRows*nCols, sizeof(uchar));
	memset(ppBg, 0, nRows*nCols);

	// Initialize background image
	printf("Extracting Background Image...\n");
	for (int index = startFrame; index < frameNumbers; index += step) {
		cvSetCaptureProperty(capture, CV_CAP_PROP_POS_FRAMES, index);
		frame = cvQueryFrame(capture);
		cvCvtColor(frame, greyframe, CV_BGR2GRAY);
		frameData = (uchar *)greyframe->imageData;
		for (int i = 0; i < pixs; i++) {
			if (frameData[i] > ppBg[i]) {
				ppBg[i] = frameData[i];
			}
		}
	}

	cvSetCaptureProperty(capture, CV_CAP_PROP_POS_FRAMES, startFrame);
	frame = cvQueryFrame(capture);
	cvCvtColor(frame, greyframe, CV_BGR2GRAY);

	// Calculate Generic Boundaries
	int boundaryCount = sdGeneric(greyframe, boundaryX, boundaryY);

	// Create Snout Ring
	IplImage *RingMask = cvCreateImage(
		cvSize(nCols, nRows), IPL_DEPTH_8U, 1);


	IplImage *distanceMask = cvCreateImage(
		cvSize(nCols, nRows), IPL_DEPTH_32F, 1);
	cvSetZero(RingMask);

	initializeFromParametersSnoutTemplate(snoutCenterX, snoutCenterY,
		snoutTipX, snoutTipY, nRows, nCols,
		boundaryX, boundaryY, boundaryCount, RingMask, distanceMask);

	// Iterate through all video frames
	cvSetCaptureProperty(capture, CV_CAP_PROP_POS_FRAMES, startFrame);

	// Aquire bwshape data
	frame = cvQueryFrame(capture);
	cvCvtColor(frame, greyframe, CV_BGR2GRAY);

	IplConvKernel *erodeDisk = cvCreateStructuringElementEx(razorSize * 2 - 1,
		razorSize * 2 - 1, 0, 0, CV_SHAPE_ELLIPSE, NULL);

	IplImage *bwShape = cvCloneImage(greyframe);
	stExtractEnvGrayValuesWithMorphology(bwShape, bwThreshold, erodeDisk);

	uchar *bwShapeData;
	bwShapeData = (uchar *)bwShape->imageData;


	cvSetCaptureProperty(capture, CV_CAP_PROP_POS_FRAMES, startFrame);

	int sourceX[20000], sourceY[20000], sinkX[20000], sinkY[20000];
	int selectedWKCount[1000];

	IplImage *ppgreyFrames[160];
	for (int ffi = 0; ffi < 1; ffi++) {
		ppgreyFrames[ffi] = cvCreateImage(
			cvSize(nCols, nRows), IPL_DEPTH_8U, 1);
	}

	int fi = 0;

	//clock_t start = clock(), diff;
	//double start_time = omp_get_wtime();
	//int threadNum = omp_get_max_threads();

	for (int segcount = 0; segcount < 1; segcount++) {

		printf("Processing Segment %d\n", segcount);

		for (int ffi = 0; ffi < 1; ffi++) {
			frame = cvQueryFrame(capture);
			cvCvtColor(frame, ppgreyFrames[ffi], CV_BGR2GRAY);
		}

		//#pragma omp parallel for private(fi)
		for (fi = 0; fi < 1; fi++) {
			//printf("Processing Frame %d\n", fi);
			// Extract Whisker Images
			ppExtractionAndFilter(ppgreyFrames[fi], pixs, ppBg, bwShapeData);

			// Calculate Mean Angle
			selectedWKCount[fi] = wdIgorMeanAngleProcess(ppgreyFrames[fi], RingMask,
				nCols, nRows, pixs, 389, 145,
				distanceMask, sourceX, sourceY, sinkX, sinkY, fi);

		}

		for (int ffi = 0; ffi < 1; ffi++) {
			fprintf(file1, "---\n");
			long tempbase = ffi * 20;
			for (int i = 0; i < selectedWKCount[ffi]; i++) {

				fprintf(file1, "%d\t%d\t%d\t%d\n",
					sourceY[tempbase + i], sourceX[tempbase + i],
					sinkY[tempbase + i], sinkX[tempbase + i]);
			}
		}

	}



	//double time = omp_get_wtime() - start_time;

	//printf("Processed 10000 frames \n");
	//printf("Time taken %.16g seconds\n", time);

	// release the image
	cvReleaseImage(&greyframe);
	cvReleaseImage(&RingMask);
	cvReleaseCapture(&capture);
	cvReleaseImage(&distanceMask);

	for (int ffi = 0; ffi < 1; ffi++) {
		cvReleaseImage(&ppgreyFrames[ffi]);
	}


	free(ppBg);
	fclose(file1);

	return 0;
}
