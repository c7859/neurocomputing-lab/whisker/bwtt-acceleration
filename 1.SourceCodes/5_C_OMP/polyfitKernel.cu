__global__ void polyfitKernel(unsigned char * const lutFit, unsigned int * const combinationLUT, 
	const unsigned int * const iSource, const unsigned int * const iSink, const double * const lutX, 
	const double * const lutY, const double * const lutRho, const double * const lutIns,
	unsigned int const pointCount, double const clusteringThreshold, unsigned int const numSources,
	unsigned int const numSinks)
{
	int indexSource = blockIdx.x * blockDim.x + threadIdx.x;;
	int indexSink = blockIdx.y * blockDim.y + threadIdx.y;
	if(indexSource >= numSources || indexSink >= numSinks){
		return;
	}

	unsigned int select[1000];

	unsigned int so = iSource[indexSource] - 1;
	unsigned int si = iSink[indexSink] - 1;

	double px = (lutX[si] - lutX[so]) / (lutRho[si] - lutRho[so]);
	double bx = lutX[so] - px*lutRho[so];

	double py = (lutY[si] - lutY[so]) / (lutRho[si] - lutRho[so]);
	double by = lutY[so] - py*lutRho[so];

	int base = indexSource + indexSink*numSources;
	int sum = 0;
	double mean = 0;
	double std = 0;

	for(int i = 0; i < pointCount; i++){
		double errorx = px*lutRho[i]+bx-lutX[i];
		double errory = py*lutRho[i]+by-lutY[i];
		double err = errorx*errorx + errory*errory;
		if(err<clusteringThreshold && lutRho[i]<lutRho[si] && lutRho[i]>lutRho[so]){
			lutFit[base*pointCount + i] = 1;
			//select[sum] = i;
			//sum++;
			//mean += lutIns[i]; 
		}
	}

	/*
	if(sum!=0){
		//mean = mean/sum;
	}

	for(int i = 0; i < sum; i++){
		lutFit[base*pointCount + select[i]] = sum;
		//std += pow(lutIns[select[i]] - mean, 2);
	}

	if(sum > 1){
		//std = sqrtf(std/(sum-1));
	}
	else
		//std = 0;

		*/

	combinationLUT[base*4] = so+1;
	combinationLUT[base*4+1] = si+1;
	combinationLUT[base*4+2] = round(255*mean);
	combinationLUT[base*4+3] = round(255*std);
}