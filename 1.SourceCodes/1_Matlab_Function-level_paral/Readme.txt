This is an improved version of BWTT using data-level parallelism.

The major changes were made in the files in the following folders

\plugin\method\wdIgorMeanAngle
\plugin\method\stShapeSpaceKalman
\plugin\method\sdGeneric