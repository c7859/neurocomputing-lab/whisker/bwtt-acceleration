
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



function uids = stUndeleteGUI(foldername)

% trash
trashpath = [foldername '/trash'];
d = dir([trashpath '/*.stJob']);
videoFilenames = {};
uids = {};
for n = 1:length(d)
	filename = d(n).name;
	job = stJob([trashpath '/' filename]);
	header = job.getHeader();
	videoFilenames{n} = stProcessPath('justFilename', job.getVideoFilename());
	uids{n} = header.uid;
end

% if none
if isempty(uids)
	stMessage('There are no jobs in the trash!');
	return
end

% pars
h_parent = gcbf;
wh = [480 400];

% create
gui = stGUI(wh, h_parent, 'Select jobs to undelete', 'margin', 8);
rect = gui.rootPanel;
h_dialog = gui.getMainWindow();

% % message
% h_text = uicontrol(h_dialog, ...
% 	'style', 'text', ...
% 	'hori', 'left', ...
% 	'fontname', 'Helvetica', ...
% 	'fontsize', 10, ...
% 	'string', 'Select jobs to undelete', ...
% 	'units', 'pixels' ...
% 	);

% selector
h_list = uicontrol(h_dialog, ...
	'style', 'listbox', ...
	'hori', 'left', ...
	'fontname', stUserData('listFontName'), ...
	'fontsize', stUserData('listFontSize'), ...
	'BackgroundColor', [1 1 1], ...
	'string', videoFilenames, ...
	'Min', 1, 'Max', 3, ...
	'units', 'pixels' ...
	);

% buttons
buttons = {'OK' 'Cancel'};
bw = 64;
for b = 1:length(buttons)
	h_buttons(1, b) = uicontrol(h_dialog, ...
		'style', 'pushbutton', ...
		'fontname', 'Helvetica', ...
		'fontsize', 10, ...
		'userdata', b, ...
		'callback', @button_callback, ...
		'string', buttons{b}, ...
		'units', 'pixels' ...
		);
end


% layout
buts = rect.pack([], 'bottom', 32);
remainder = rect.pack(h_list, 'bottom', -1);
% label = remainder.pack(h_text, 'top', 16);
% remainder.pack(h_list, 'top', -1);
for b = 1:length(h_buttons)
	buts.pack(h_buttons(b), 'right', bw);
end

% focus
uicontrol(h_list);

% wait
gui.waitForClose();






	function button_callback(h_button, evt)
		
		response = buttons{get(h_button, 'userdata')};
 		if strcmp(response, 'OK') && ~isempty(uids)
			uids = uids(get(h_list, 'Value'));
		else
			uids = {};
		end
		gui.close();
		
	end



end




