function result = stPlugin(method, operation, varargin)

% output = stPlugin(method, operation, input)
%
%   This form calls the plugin called "method", with
%   appropriate paths set before the call is made, and with
%   the output "normalised" after the call is made.
%   Normalisation of the output means that missing fields
%   are added, using default values.
%
%
%
% plugins = stPlugin()
%
%   This form returns a directory of available plugins. This
%   directory is only refreshed if you restart Matlab, or
%   call "clear all".
%
%
%
% See also: stRevision, stProject, stJob, stPlugin, stMergePars, stUserData, stLoadMovieFrames.


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% Author: Ben Mitch
% Created: 01/03/2011
%
% Because the world is pretty and sunny and
% filled with little rabbits and other beautiful things,
% shame on you if you make it less beautiful. But do as
% you will with this code, the little rabbits will not
% mind.



persistent cache

% handle debug method
debug = false;
getdir = nargin == 0;
if nargin == 1 && ischar(method) && strcmp(method, 'debug')
	cache = [];
	debug = true;
	getdir = true;
end



%% BUILD CACHE

if isempty(cache)
	
	% basics
	cache.installPath = stGetInstallPath();
	cache.methodPath = [cache.installPath '/plugin/method'];
	cache.outputPath = [cache.installPath '/plugin/output'];
	cache.toolPath = [cache.installPath '/plugin/tool'];
	cache.sharedPath = [cache.installPath '/plugin/shared'];
	cache.plugins = struct();
	cache.debug = debug;
	
	% add paths
	addpath(cache.methodPath);
	addpath(cache.outputPath);
	addpath(cache.toolPath);
	addpath(cache.sharedPath);

	% empty cache
	cache.dir = [];
	
	% method directory
	d = dir([cache.methodPath '/*.m']);
	for n = 1:length(d)
		funcname = d(n).name(1:end-2);
		role = funcname(1:2);
		try
			info = stPlugin(funcname, 'info');
			if cache.debug
				pars = stPlugin(funcname, 'parameters');
			end
			cache.dir.(role).(funcname) = info;
		catch err
			disp(getReport(err));
			warning(['plugin "' funcname '" did not load correctly']);
		end
	end
	
	% output directory
	d = dir([cache.outputPath '/*.m']);
	for n = 1:length(d)
		funcname = d(n).name(1:end-2);
		role = funcname(1:2);
		try
			info = stPlugin(funcname, 'info');
			if cache.debug
				pars = stPlugin(funcname, 'parameters');
			end
			cache.dir.(role).(funcname) = info;
		catch err
			disp(getReport(err));
			warning(['plugin "' funcname '" did not load correctly']);
		end
	end
	
	% tool directory
	d = dir([cache.toolPath '/st*.m']);
	for n = 1:length(d)
		funcname = d(n).name(1:end-2);
		role = 'tool';
		try
			info = stPlugin(funcname, 'info');
			if cache.debug
				pars = stPlugin(funcname, 'parameters');
			end
			cache.dir.(role).(funcname) = info;
		catch err
			disp(getReport(err));
			warning(['plugin "' funcname '" did not load correctly']);
		end
	end
	
	% create lists
	roles = {'pp' 'od' 'ot' 'sd' 'st' 'wd' 'wt' 'om' 'tool'};
	for r = 1:length(roles)
		role = roles{r};
		if isfield(cache.dir, role)
			list = fieldnames(cache.dir.(role));
		else
			list = {};
		end
		cache.dir.(role).list = list;
	end
	
end



%% DIRECTORY

if getdir
	
	% get result
	result = cache.dir;
	
	% clear debug flag
	cache.debug = false;
	
	% ok
	return
	
end



%% CALL

if ~isfield(cache.plugins, method)
	
	% detect function
	detected = which(method);
	if isempty(detected)
		error(['plugin "' method '" was not found and cannot be run']);
	end
	
	% detect where it lives
	plugin.path = fileparts(detected);
	if stCompareFilenames(plugin.path, cache.methodPath)
		plugin.type = 'method';
	elseif stCompareFilenames(plugin.path, cache.outputPath)
		plugin.type = 'output';
	elseif stCompareFilenames(plugin.path, cache.toolPath)
		plugin.type = 'tool';
	else
		error(['plugin "' method '" is shadowed by "' detected '" and cannot be run']);
	end
	
	% create plugin data object
	plugin.func = str2func(method);
	plugin.suppdir = [plugin.path '/' method];
	plugin.suppdir(plugin.suppdir == '/') = filesep;
	if ~exist(plugin.suppdir, 'dir')
		plugin.suppdir = '';
	end
	
	% store plugin
	cache.plugins.(method) = plugin;
	
end

cwd = cd;

try
	plugin = cache.plugins.(method);
	if ~isempty(plugin.suppdir)
		cd(plugin.suppdir)
	end
	result = plugin.func(operation, varargin{:});
	cd(cwd)
catch err
	cd(cwd)
	rethrow(err);
end



%% NORMALISE OUTPUT

switch operation
	
	case 'info'
		result = ensureField(result, 'flags', {});
		
	case 'parameters'
		
		% for now, we just promote the "pars" field - if we need
		% to extend this interface in future, we'll stop doing
		% this and callers to stPlugin will have to handle the
		% change
		if ~isempty(result) && isfield(result, 'pars')
			result = result.pars;
		end
		
		for p = 1:length(result)
			
			par = result{p};
			
			% reveal inferred parameter types
			if cache.debug
				if ~isfield(par, 'type')
					disp(['INFERRED CONSTANT TYPE: ' method ' : ' par.name])
				end
			end
			
			par = ensureFieldPar(par, 'type', 'constant');
			par = ensureFieldPar(par, 'label', par.name);
			par = ensureFieldPar(par, 'help', '');
			par = ensureFieldPar(par, 'show', '');
			par = ensureFieldPar(par, 'propagate', true);
			
			switch par.type
				case 'scalar'
					par = ensureFieldPar(par, 'precision', 0);
					par = ensureFieldPar(par, 'step', 10^(-par.precision)*[1 5]);
					par = ensureFieldPar(par, 'constraints', {});
			end
			
			result{p} = par;
			
		end
		
end





	function s = ensureField(s, n, v)
		
		if ~isfield(s, n)
			if cache.debug
				disp(['SUPPLIED DEFAULT INFO FIELD: ' method ' : ' n])
			end
			s.(n) = v;
		end
		
	end


	function s = ensureFieldPar(s, n, v)
		
		if ~isfield(s, n)
			if cache.debug
				disp(['SUPPLIED DEFAULT FIELD: ' method ' : ' s.name '.' n])
			end
			s.(n) = v;
		end
		
	end


end

