
% a = stStringExplode(c, s, q)
%   a is a cell array of items exploded from the string s
%   using the character c as delimiter. if q is supplied, it
%   should be the quote character. delimiters inside a pair
%   of quotes are ignored.


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function a = stStringExplode(c, s, q)

if nargin < 3
	q = char(0);
end


a = {};

if ~ischar(c) || ndims(c)~=2 || size(c, 1)~=1 || size(c, 2)~=1
	error('BWTT:stStringExplode:InvalidArguments', ...
		['c is invalid in stStringExplode()']);
end

if ~isempty(s) && (~ischar(s) || ndims(s)~=2 || size(s, 1)~=1)
	error('BWTT:stStringExplode:InvalidArguments', ...
		['s is invalid in stStringExplode()']);
end

if ~ischar(q) || ndims(q)~=2 || size(q, 1)~=1 || size(q, 2)~=1
	error('BWTT:stStringExplode:InvalidArguments', ...
		['q is invalid in stStringExplode()']);
end


% find when we're inside quotes
q = mod(cumsum(s == q), 2);

% convert
f = find((s == c) & ~q);
f = [0 f length(s)+1];
for n = 2:length(f)
	a{n-1} = s(f(n-1)+1:f(n)-1);
end


