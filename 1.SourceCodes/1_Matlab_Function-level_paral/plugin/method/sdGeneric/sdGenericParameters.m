
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function pars = sdGenericParameters()

% sdGenericParameters.m
%
% Outputs:
%           pars is a struct with the following parameters
%           *) pars.bwThreshold selects the inital threshold for whisker
%           detection
%           *) pars.bwMorphology is the threshold for setting the importance of
%           detected features, usually 5/255
%           *) pars.razorSize measures the width of detected snout mask
%
%*************************************************************************
%  Author: Perkon Igor    -    perkon@sissa.it



% empty
pars = {};

% parameter
par = [];
par.name = 'bwThreshold';
par.type = 'scalar';
par.label = 'Black/White Threshold';
par.help = 'This is where you can put some basic help text';
par.range = [0.01 1];
par.step = [0.01 0.05];
par.precision = 2;
par.value = 0.2;
pars{end+1} = par;

% parameter
par = [];
par.name = 'bwMorphology';
par.type = 'scalar';
par.label = 'Black/White Morphology';
par.range = [0 1];
par.step = [0.01 0.05];
par.precision = 2;
par.value = 0.02;
pars{end+1} = par;

% parameter
par = [];
par.name = 'razorSize';
par.type = 'scalar';
par.label = 'Razor Size';
par.range = [1 40];
par.step = [1 3];
par.value = 5;
pars{end+1} = par;




