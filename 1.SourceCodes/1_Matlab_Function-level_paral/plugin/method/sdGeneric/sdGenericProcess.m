
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



% function [snoutContour, nextChunkData, optionalOutputs] = sdGeneric(imageBuffer, sdparam, previousChunkData)

function state = sdGenericProcess(job, state)

% sdGeneric.m
% syntax:
%       [snoutContour, optionalOutputs] = sdGeneric(imageBuffer, param)
%
% Inputs:
%           - imageBuffer is the buffer contining input images. It can be
%           a single frame or a buffer or frames
%           - sdparam is a struct with the following parameters
%           param.bwThreshold selects the inital threshold for whisker
%           detection
%           param.bwMorphology is the threshold for setting the importance of
%           detected features, usually 5/255
%           param.razorSize measures the width of detected snout
%           - packedBWmask is an optional argument, containing the packed
%           version of the ROI mask. Use bwunpack to extract it
% Outputs:  - snoutContour is the cell array containing the x-y coordinates of
%             detected snout outlines. For multiple view tracking,
%             snoutContour is a cell matrix with number of rows equal to
%             the number of frames and number of columns equal to the
%             number of views. If there is only one image
%             in the buffer, the output is a two-dimensional x-y array.
%          - nextChunkData you pass the data used for the next iteration
%          - optionalOutputs is a struct containing the following
%          parameters:
%           - optionalOutputs.packedBWshape is the shape of the rats snout in its
%           packed version
%
%*************************************************************************
%  Author: Perkon Igor    -    perkon@sissa.it

sdContour = state.sdContour;

if isempty(sdContour) 
    % get the image
    GRAYshape = job.getRuntimeFrame(state.frameIndex);

    % do the BW thing
    BWmask = ones(size(GRAYshape{1}));
    [GRAYwhiskers, BWshape] = ...
        stExtractWhiskersGrayValuesWithMorphologyAndShave( ...
        uint8(GRAYshape{1}), ...
        state.pars.bwThreshold, ...
        state.pars.bwMorphology, ...
        state.pars.razorSize, ...
        BWmask ...
        );

    % do the boundaries thing
    %[gx,gy] = gradient(BWshape);
    %contourImage = ((gx.^2 + gy.^2)>0).*BWshape;
    %[x,y] = find(contourImage);
    %sdContour = [x';y'];
    bdrs = bwboundaries(BWshape);
    sdContour = [];
    for i = 1:numel(bdrs),
        sdContour = [sdContour,bdrs{i}'];
    end
    state.sdContour = sdContour;
end

% store the result for this frame
job.setRuntimeState(state.frameIndex, 'sdContour', state.sdContour);
end

% optionalOutputs = struct('packedBWshape', bwpack(BWshape));




