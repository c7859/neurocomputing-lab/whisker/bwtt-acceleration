
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



function state = wdGVMProcess(job, state)



%% RETRIEVE STATE

% assume one frame at a time, for now
frameIndex = state.frameIndex;

% may as well alert user if we fail on no preproc, since it
% takes a long time to compute this in the error condition
if ~job.isPreprocPresent(frameIndex)
	error('cannot compute wdIgorMeanAngle - no pre-processing is configured (recommend ppBgExtractionAndFilter)');
end

% get the image
imageBuffer = job.getRuntimeFrame(frameIndex);

% this isn't efficient, but it's easy...
imageBuffer = imageBuffer{1};

% get snoutTrackingData
results = job.getResults(frameIndex);
if ~isfield(results{1}, 'snout')
	error('cannot compute wdGVM - no snout tracking available');
end
stdata = results{1}.snout;

% decompress
stdata.perpdists = double(stdata.perpdists);
stdata.contour = double(stdata.contour);
snoutTrackingData = stdata;

% get pars
pars = state.pars;

% retrieve state
previousWDChunkData = state;







%% GVM CODE

% This function detects the whiskers by first transforming to polar
% coordinates.
whiskerContour = [];
nextWDChunkData = previousWDChunkData;

if ~isempty(snoutTrackingData.contour)
	
	plotPolarWhisker = 0; % a local variable for debugging
	if snoutTrackingData.orientation < -pi
		snoutTrackingData.orientation = snoutTrackingData.orientation + 2*pi;
	elseif snoutTrackingData.orientation > pi
		snoutTrackingData.orientation = snoutTrackingData.orientation - 2*pi;
	end;
	
	snoutTrackingData.orientation = snoutTrackingData.orientation + pi/2;
	%
	%     % Setup local parameters
	%     maximaParam{1} = wdParameters.Variable{1}.value;
	%     maximaParam{2} = round(wdParameters.Constant{2}.value/8);
	%
	%     followParam.minWhiskerLength = wdParameters.Variable{3}.value;
	%     followParam.threshold = wdParameters.Variable{1}.value;
	%     followParam.jumpRight = wdParameters.Constant{7}.value;
	%     followParam.jumpLeft = wdParameters.Constant{8}.value;
	%     followParam.leftParam{1} = wdParameters.Variable{1}.value;
	%     followParam.leftParam{2} = [wdParameters.Constant{6}.value,wdParameters.Constant{6}.value];% angle spread
	%     followParam.leftParam{3} = wdParameters.Constant{5}.value; % angle jumps
	%     followParam.leftParam{4} = wdParameters.Variable{2}.value;
	%     followParam.rightParam{1} = wdParameters.Variable{1}.value/2;
	%     followParam.rightParam{2} = [wdParameters.Constant{6}.value,wdParameters.Constant{6}.value];
	%     followParam.rightParam{3} = wdParameters.Constant{5}.value;
	%     followParam.rightParam{4} = wdParameters.Variable{2}.value;
	%
	
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%   GVM begin
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	vectorLeftX = [];
	vectorLeftY = [];
	vectorRightX = [];
	vectorRightY = [];
	
	leftWhiskers = [];
	rightWhiskers = [];
	leftBasePointsX = [];
	leftBasePointsY = [];
	rightBasePointsX = [];
	rightBasePointsY = [];
	% if(~isempty(snoutTrackingData.contour))
	img = uint8(imageBuffer);
	% end;
	imHist = imhist(img);
	[imHistMax imIdx]=max(imHist(20:255));
	amp = 0.82*200/imIdx;
	mask = zeros([1024, 1024],'uint8');
	img2 = amp*img;
	
	warning off;
	
	% pars = [];
	pars.whiskerCentre = snoutTrackingData.center;
	pars.headBearing = snoutTrackingData.orientation;
	
	% pars.erodeRadius = wdParameters.Variable{1}.value;          % default 6
	% pars.radInv = wdParameters.Variable{2}.value;               % default 160
	% pars.hhThreshold = wdParameters.Variable{3}.value;          % default 160
	% pars.erodeBtoW = wdParameters.Variable{4}.value;            % default 10
	% pars.edgeDetectLevelNum = wdParameters.Variable{5}.value;   % default 32
	
	[out, debug] = GVM030610(img2, mask, pars);
	
	outMat = out';
	s = size(outMat);
	
	thetas = outMat(:,3);
	
	% wrap to -180 -> 180
	thetas(thetas > 180) = thetas(thetas > 180) - 360;
	thetas(thetas < -180) = thetas(thetas < -180) + 360;
	
	% store back
	outMat(:,3) = thetas;
	
	whiskerData.outMat = outMat(:,1:3); % relative to head bearing
	whiskerData.outMat(:, 4) = thetas + snoutTrackingData.orientation*180/pi - 90; % relative to image space
	whiskerData.outMat(:, 5) = 0 * whiskerData.outMat(:, 4); % space to record (later) whether whisker is left/right
	
	% for i=1:length(thetas)
	%     nextWDChunkData.angles = num2cell(thetas);
	% end
	
	
	
	for j = 1:length(whiskerData.outMat(:,3))
		
		% get angle of base of whisker from head center
		bx = whiskerData.outMat(j,2);
		by = whiskerData.outMat(j,1);
		v = [bx; by] - [snoutTrackingData.center(1); snoutTrackingData.center(2)];
		vang = atan2(-v(2), v(1));
		
		% compare with head angle
		vang = vang - (snoutTrackingData.orientation - pi/2);
		vang(vang > pi) = vang(vang > pi) - 2*pi;
		vang(vang < -pi) = vang(vang < -pi) + 2*pi;
		
		if vang > 0
			whiskerData.outMat(j, 5) = 1;
			leftWhiskers = [leftWhiskers, whiskerData.outMat(j,3)];
			leftBasePointsX = [leftBasePointsX whiskerData.outMat(j,1)];
			leftBasePointsY = [leftBasePointsY whiskerData.outMat(j,2)];
		else
			whiskerData.outMat(j, 5) = 2;
			rightWhiskers = [rightWhiskers, whiskerData.outMat(j,3)];
			rightBasePointsX = [rightBasePointsX whiskerData.outMat(j,1)];
			rightBasePointsY = [rightBasePointsY whiskerData.outMat(j,2)];
		end
		
		
		
		
		meanLeftWhiskers = mean(leftWhiskers);
		meanRightWhiskers = mean(rightWhiskers);
		meanLeftBasePointsX = mean(leftBasePointsX);
		meanLeftBasePointsY = mean(leftBasePointsY);
		meanRightBasePointsX = mean(rightBasePointsX);
		meanRightBasePointsY = mean(rightBasePointsY);
		
		l = 150;
		lw = 4;
		
		bx = whiskerData.outMat(j,2);
		by = whiskerData.outMat(j,1);
		wx = cosd(whiskerData.outMat(j,4)) * l;
		wy = -sind(whiskerData.outMat(j,4)) * l;
		is_left = whiskerData.outMat(j,5) == 1;
		
		ol = meanLeftWhiskers + snoutTrackingData.orientation*180/pi-90;
		or = meanRightWhiskers + snoutTrackingData.orientation*180/pi-90;
		
		blx = meanLeftBasePointsY;
		bly = meanLeftBasePointsX;
		
		brx = meanRightBasePointsY;
		bry = meanRightBasePointsX;
		
		% figure(12)
		% imshow(imageBuffer)
		% hold on
		% plot(snoutTrackingData.center(2),snoutTrackingData.center(1), '.b')
		% hold on
		% if is_left
		%     plot([bx bx+wx], [by by+wy], 'color', 0.75*[0.75 1 0.75])
		% else
		%     plot([bx bx+wx], [by by+wy], 'color', 0.75*[1 0.75 0.75])
		% end
		% hold on
		% vectorLeftX{j} = {blx:blx+cosd(ol)*l};
		% vectorLeftY{j} = {bly:bly-sind(ol)*l};
		%
		% vectorRightX = [vectorRightX brx+cosd(or)*l];
		% vectorRightY = [vectorRightY bry-sind(or)*l];
		% whiskerContour{j} = [[bx bx+wx];[by by+wy]];
		% whiskerContour{j} = [[bly bly-sind(ol)*l];[blx blx+cosd(ol)*l]];
		if is_left
			whiskerContour{j} = [[by by+wy];[bx bx+wx]];
		else
			whiskerContour{j} = [[by by+wy];[bx bx+wx]];
		end
		% whiskerContour{j} = [[bry bry-sind(or)*l];[brx brx+cosd(or)*l]];
	end
	
	% plot(bx, by, '.b')
	% hold on% plot([blx blx+cosd(ol)*l], [bly bly-sind(ol)*l], '-g', 'linewidth', lw);
	% hold on
	% plot([brx brx+cosd(or)*l], [bry bry-sind(or)*l], '-r', 'linewidth', lw);
	% hold on
	
	
	
	
	%   clear imageBuffer;
	%     % Head coordinates originating at the snout contour
	%     [ang, rad] = cart2pol(snoutTrackingData.contour(1,:)-snoutTrackingData.center(1), ...
	%         snoutTrackingData.contour(2,:)-snoutTrackingData.center(2));
	%     if plotPolarWhisker ==1;figure;plot(ang, rad);end;
	%     if plotPolarWhisker ==1;figure;imagesc(imageBuffer); hold on;
	%         plot(snoutTrackingData.contour(2,:), snoutTrackingData.contour(1,:),'w', 'linewidth', 3.0);
	%         plot(snoutTrackingData.center(2), snoutTrackingData.center(1),'xw', 'markersize', 3.0);end;
	%
	%     % Transform imageBuffer to polar coordinates
	%     angles = ((-wdParameters.Variable{6}.value*pi):(2*pi/wdParameters.Constant{2}.value):(wdParameters.Variable{6}.value*pi));% - snoutTrackingData.orientation;
	%     radii = 1:wdParameters.Variable{4}.value;
	%
	%     localRadius = zeros(size(angles));
	%     for iAngles = 1:length(angles)
	%         nAngle = angles(iAngles) + snoutTrackingData.orientation;
	%         nAngle = nAngle + 2*pi .* (nAngle < -pi) -2*pi .* (nAngle > pi);
	%         [minAng, fAng] = min(abs(ang - nAngle));
	%         localRadius(iAngles) = rad(fAng);
	%     end;
	%     if plotPolarWhisker ==1;figure;plot(angles, localRadius);end;
	%
	%     polarImage = zeros(length(angles), length(radii));
	%     for iRadius=1:length(radii)
	%         [boundr(:,1), boundr(:,2)] = pol2cart(angles + snoutTrackingData.orientation, radii(iRadius) + localRadius);
	%         boundr(:,1) = round(boundr(:,1) + snoutTrackingData.center(1));
	%         boundr(:,2) = round(boundr(:,2) + snoutTrackingData.center(2));
	%         boundr(:,1) = boundr(:,1).* ( (boundr(:,1) > 0) & ((boundr(:,1) < size(imageBuffer,1)))) + ...
	%             (boundr(:,1) <= 0) + size(imageBuffer,1).*(boundr(:,1) >= size(imageBuffer,1));
	%         boundr(:,2) = boundr(:,2).* ( (boundr(:,2) > 0) & ((boundr(:,2) < size(imageBuffer,2)))) + ...
	%             (boundr(:,2) <= 0) + size(imageBuffer,2).*(boundr(:,2) >= size(imageBuffer,2));
	%         polarImage(:, iRadius) = imageBuffer(sub2ind(size(imageBuffer), boundr(:,1), boundr(:,2)));
	%     end;
	%
	%     % Follow whiskers in polar coordinates
	%     % Search in several radii, find tha maxima and follow the whiskers
	%     if plotPolarWhisker ==1; figure;imagesc(polarImage);hold on; end; % debugging
	%
	%     allWhiskers = cell(length(wdParameters.Constant{3}.value), 1);
	%     for krf = 1:length(wdParameters.Constant{3}.value)
	%         maximaParam{1} = wdParameters.Variable{1}.value / sqrt(krf);
	%
	%         rf=wdParameters.Constant{3}.value(krf);
	%         polarLine = polarImage(:,rf);
	%         foundMaxima = FindLikelyMaxima(polarLine, maximaParam);
	%         for kchmax=1:length(foundMaxima); foundMaxima{kchmax}(2) = rf; end;
	%         allWhiskers{krf} = followWhiskerSnoutBased(polarImage, foundMaxima, followParam);
	%     end;
	%
	%
	%     % prune out unfit whiskers in polar coordinates
	%     whiskerPolar = [];
	%     for krf = 1:length(wdParameters.Constant{3}.value)
	%         for kw=1:length(allWhiskers{krf})
	%             if(length(whiskerPolar) < wdParameters.Constant{1}.value)
	%                 if(length(allWhiskers{krf}{kw}) > wdParameters.Variable{3}.value)
	%                     whiskerPolar{end+1} = allWhiskers{krf}{kw};
	%                     if plotPolarWhisker ==1;plot(whiskerPolar{end}(1,:), whiskerPolar{end}(2,:), 'w');hold on;end;
	%                 end;
	%             else
	%                 break;
	%             end;
	%         end;
	%         if(length(whiskerPolar) >= wdParameters.Constant{1}.value)
	%             %         disp('Too many whiskers');break;
	%         end;
	%     end;
	%
	%
	%     % Transform back to XY coordinates
	%     whiskerXY = cell(length(whiskerPolar), 1);
	%     for iWhiskers = 1:length(whiskerPolar)
	%         [whiskerXY{iWhiskers}(1, :), whiskerXY{iWhiskers}(2, :)] = pol2cart(angles(whiskerPolar{iWhiskers}(2, :)) + snoutTrackingData.orientation, ...
	%             radii(whiskerPolar{iWhiskers}(1, :)) + localRadius(whiskerPolar{iWhiskers}(2, :)));
	%         whiskerXY{iWhiskers}(1, :) = whiskerXY{iWhiskers}(1 ,:) + snoutTrackingData.center(1);
	%         whiskerXY{iWhiskers}(2, :) = whiskerXY{iWhiskers}(2, :) + snoutTrackingData.center(2);
	%     end;
	%
	%     % prune unfit whiskers
	%     whiskerXY = filterWhiskersByShape(whiskerXY, wdParameters.Constant{4}.value , 'maxErrorOnWholeShape');
	%
	%     % Smooth whiskers in XY coordinate
	%     for iWhiskers = 1:length(whiskerXY)
	%         straightWhiskersX = round(colfilt(whiskerXY{iWhiskers}(1, :), [1 10],  'sliding', @mean));
	%         straightWhiskersY = round(colfilt(whiskerXY{iWhiskers}(2, :), [1 10],  'sliding', @mean));
	%         whiskerXY{iWhiskers}(1, 10:(end-10)) = straightWhiskersX(10:(end-10));
	%         whiskerXY{iWhiskers}(2, 10:(end-10)) = straightWhiskersY(10:(end-10));
	%     end;
	%
	%     % prune unfit whiskers
	%     whiskerXY = filterWhiskersByShape(whiskerXY, wdParameters.Constant{4}.value , 'maxErrorOnWholeShape');
	%
	%     % Dilute whiskers
	%     if ~isempty(whiskerXY)
	%         diluteWhiskers{1} = whiskerXY{1};
	%         for iWhiskers = 2:length(whiskerXY)
	%             closeWhisker = 0;
	%             for kWhiskers = 1:length(diluteWhiskers)
	%                 minLength = min([length(whiskerXY{iWhiskers}),length(diluteWhiskers{kWhiskers})]);
	%                 distance = mean(sqrt(sum( (whiskerXY{iWhiskers}(:, 1:minLength) - diluteWhiskers{kWhiskers}(:, 1:minLength)).^2, 1)));
	%                 if(distance < wdParameters.Variable{5}.value)
	%                     closeWhisker = 1;
	%                     break;
	%                 end;
	%             end;
	%             if ~closeWhisker
	%                 diluteWhiskers{end+1} = whiskerXY{iWhiskers};
	%             end;
	%         end;
	%
	%         for kWhiskers = 1:length(diluteWhiskers)
	%             diluteWhiskers{kWhiskers} = diluteWhiskers{kWhiskers};
	%         end;
	%     else
	%         diluteWhiskers = [];
	%     end;
	%     whiskerContour = {[bx bx+wx], [by by+wy]}
	%     % nextWDChunkData = previousWDChunkData;
	%     nextWDChunkData.localRadiusCurrent = localRadius;
	%     if(isempty(previousWDChunkData.localRadiusCurrent))
	%         nextWDChunkData.localRadiusPrev =localRadius;
	%     else
	%         nextWDChunkData.localRadiusPrev = previousWDChunkData.localRadiusCurrent;
	%     end;
	
	% nextWDChunkData.snoutCenterCurrent = snoutTrackingData.center;
	%     if(isempty(previousWDChunkData.snoutCenterCurrent))
	%         nextWDChunkData.snoutCenterPrev = snoutTrackingData.center;
	%     else
	%         nextWDChunkData.snoutCenterPrev = previousWDChunkData.snoutCenterCurrent;
	% 		end
	
end












%% STORE RESULTS

% compress results
for w = 1:length(whiskerContour)
	whiskerContour{w} = single(whiskerContour{w});
end

% store results
results = [];
results.contour = whiskerContour;
job.setResults(frameIndex, 'whisker', results);





