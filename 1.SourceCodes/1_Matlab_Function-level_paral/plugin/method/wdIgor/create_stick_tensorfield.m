
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function T = stickField(uv, sigma)

% Calculate the window size from sigma using
% equation 5.7 from Emerging Topics in Computer Vision
% make the field odd, if it turns out to be even.
ws = floor( ceil(sqrt(-log(0.01)*sigma^2)*2) / 2 )*2 + 1;
whalf = (ws-1)/2;

% Turn the unit vector into a rotation matrix
rot = [uv(:),[-uv(2);uv(1)]]/norm(uv);
btheta = atan2(uv(2),uv(1));

% Generate our theta's at each point in the
% field, adjust by our base theta so we rotate
% in funcion.
[X,Y] = meshgrid(-whalf:1:whalf,whalf:-1:-whalf);
Z = rot'*[X(:),Y(:)]';
X = reshape( Z(1,:),ws,ws);
Y = reshape( Z(2,:),ws,ws);
theta = atan2(Y,X);

% Generate the tensor field direction aligned with the normal
Tb = reshape([theta,theta,theta,theta],ws,ws,2,2);
T1 = -sin(2*Tb+btheta);
T2 = cos(2*Tb+btheta);
T3 = T1;
T4 = T2;
T1(:,:,2,1:2) = 1;
T2(:,:,1:2,1) = 1;
T3(:,:,1:2,2) = 1;
T4(:,:,1,1:2) = 1;
T = T1.*T2.*T3.*T4;


% Generate the attenuation field, taken from Equation
% 5.2 in Emerging Topics in Computer Vision. Note our
% thetas must be symmetric over the Y axis for the arc
% length to be correct so there's a bit of a coordinate
% translation.
theta = abs(theta);
theta(theta>pi/2) = pi - theta(theta>pi/2);
theta = 4*theta;

s = zeros(ws,ws);
k = zeros(ws,ws);
% Calculate the attenuation field.
l = sqrt(X.^2+Y.^2);
c = (-16*log2(0.1)*(sigma-1))/pi^2;
s(l~=0 & theta~=0) = (theta(l~=0 & theta~=0).*l(l~=0 & theta~=0))./sin(theta(l~=0 & theta~=0));
s(l==0 | theta==0) = l(l==0 | theta==0);
k(l~=0) = 2*sin(theta(l~=0))./l(l~=0);
DF = exp(-((s.^2+c*(k.^2))/sigma^2));
DF(theta>pi/2) = 0;
% Generate the final tensor field
T = T.*reshape([DF,DF,DF,DF],ws,ws,2,2);

