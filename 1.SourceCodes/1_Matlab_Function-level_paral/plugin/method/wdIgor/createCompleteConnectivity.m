
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function newConnectivityMatrix = createCompleteConnectivity(contourLUT, whiskerTreeLUTwithOrientations, radiusSize)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Inputs:
%   - whiskerTreeLUTwithOrientations is a matrix, where the data are
%           [inCurve inMarker ::::: xofmarker, yofmarker, c, theta,
%           rho,angle, roundness, saliency]
%   - contourLUT are the point of the contur data with
%   whiskerTreeLUTwithOrientations data format
%   - radiusSize represent the interpoint distance
% Outputs:
%   - newConnectivityMatrix is an [inCurve inMarker outCurve outMarker 1]
% <\outDOC>

if nargin < 3,
    radiusSize = 5;
end

%% get distance matrix
ptsax = contourLUT(:,4)';
ptsay = contourLUT(:,3)';
ptsbx = whiskerTreeLUTwithOrientations(:,4)';
ptsby = whiskerTreeLUTwithOrientations(:,3)';
%
Sa = numel(ptsax);
Sb = numel(ptsbx);
xDiff = repmat(ptsax,[Sb 1]) - repmat(ptsbx', [1 Sa]);
yDiff = repmat(ptsay,[Sb 1]) - repmat(ptsby', [1 Sa]);
% avoid sqrt for faster min searching
[thetaMatrix, rhoMatrix] = cart2pol(xDiff, yDiff);

%% get closest elements
newConnectivityMatrix = zeros(10000,5);
newConnectivityIdx = 0;
for i=1:Sa,
    [val, sortedIdx] = sort(rhoMatrix(:,i));
    for j=1:Sb,
        if val(j) < radiusSize,
            newConnectivityIdx = newConnectivityIdx + 1;
            inCurve = contourLUT(i,1);
            inMarker = contourLUT(i,2);
            outCurve = whiskerTreeLUTwithOrientations(sortedIdx(j), 1);
            outMarker = whiskerTreeLUTwithOrientations(sortedIdx(j), 2);
            newConnectivityMatrix(newConnectivityIdx,:) = [inCurve inMarker outCurve outMarker 1];
            newConnectivityIdx = newConnectivityIdx + 1;
            newConnectivityMatrix(newConnectivityIdx,:) = [outCurve outMarker inCurve inMarker  val(j)];
        else
            break;
        end
    end
end
newConnectivityMatrix = newConnectivityMatrix(1:newConnectivityIdx,:);
    
    