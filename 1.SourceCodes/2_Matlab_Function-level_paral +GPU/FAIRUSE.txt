The BIOTACT Whisker Tracking Tool (BWTT).
Copyright 2015.

RECOMMENDATIONS FOR FAIR USE OF THIS SOFTWARE

Whilst this software is in the public domain, it is the outcome of a considerable investment of time and effort. You are therefore encouraged to give appropriate credit when publishing results that rely on its use.

1) Acknowledgement. The BWTT framework need not currently be cited. You may like to acknowledge its use either as "the BIOTACT Whisker Tracking Tool (http://bwtt.sourceforge.net)" or, if you have space, as "the BIOTACT Whisker Tracking Tool, an artefact of the EU Framework 7 Project BIOTACT 215910 (http://bwtt.sourceforge.net)".

2) Citation. Details of the appropriate citations for the particular methods that you have used will be provided when you export your results. You can review all current citation information at http://bwtt.sourceforge.net/fairuse.

3) Co-authorship. Some of the provided algorithms may be unpublished, and the authors may therefore request co-authorship of the first journal publication using their algorithm. This requirement will be highlighted when you export results which have been processed by such an algorithm. You may wish to contact an author in advance to discuss this requirement. Current requirements are listed at http://bwtt.sourceforge.net/fairuse.
