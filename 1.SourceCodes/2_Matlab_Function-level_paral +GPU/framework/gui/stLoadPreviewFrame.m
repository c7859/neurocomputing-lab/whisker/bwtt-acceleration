
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function [previewFrame, videoInfo] = stLoadPreviewFrame(frameIndex, job, previewWindowSize)

% persistent preview data
persistent previewData

% get userdata (this may change, so we can't cache it)
userdata = getUserData();

% prepare state
unload = false;
reload = false;
frameData = [];

% switch on number of arguments
switch nargin
	
	case 0
		
		% this is the command to finalise. unload, store any
		% modifications to currently loaded preview object.
		unload = ~isempty(previewData);
		reload = false;
		
	case 3
		
		% this is the command to change (possibly) the currently
		% loaded preview object (it will not change if the video
		% file specified is the same, which may happen
		% sometimes).
		
		% unload if video file is not the same as the currently
		% loaded one
		videoFilename = job.getVideoFilename();
		
		% set flags
		if isempty(previewData)
			unload = false;
			reload = true;
		else
			unload = ~strcmp(previewData.videoFilename, videoFilename);
			reload = unload;
		end
		
	case 1
		
		% just frame was requested, so caller is implying that
		% the currently loaded previewData is associated with
		% the correct video file
		unload = false;
		reload = false;
		
	otherwise
		
		error('misuse of stLoadPreviewFrame()');
		
end

% unload
if unload
	
	% NB: -v6
	%
	% the -v6 option disables compression in saved files,
	% which saves a lot of time. these data are already jpeg
	% compressed anyway, so there is no mileage in trying to
	% compress them losslessly in addition, it just takes
	% time.
	
	% save state if mode is "On disk" and the object has been
	% modified
	if userdata.persist && previewData.modified
		
		% get persist filename
		previewFilename = getPreviewFilename(userdata, previewData.previewWindowSize, previewData.thehash);
		
		if isempty(previewFilename)
			warning('cannot save video preview data (temporary directory not set)');
		else
			if exist('job', 'var')
				logid = job.log('saving preview data...');
				save(previewFilename, 'previewData', '-v6');
				job.log(logid);
			else
				% when called to finalise, we are called without a
				% job. we can't therefore do logging. we could pass
				% in something else, as a log target, but who cares,
				% really.
				save(previewFilename, 'previewData', '-v6');
			end
		end
	end
	
	% unload from memory
	previewData = [];
	
end

% reload
if reload

	% get thehash
	thehash = upper(hash(videoFilename, 'MD5'));
	
	% get persist filename
	previewFilename = getPreviewFilename(userdata, previewWindowSize, thehash);
	
	% if mode is "On disk"
	if userdata.persist
		
		% load if available
		if exist(previewFilename, 'file')
			logid = job.log('loading preview data...');
			previewData = load(previewFilename);
			previewData = previewData.previewData;
			previewData.modified = false;
			job.log(logid);
		end
		
	end
	
	% if we did not load from disk, for whatever reason,
	% create a new previewData object now
	if isempty(previewData)
	
		% load a frame (only to get the info, but since we're
		% loading a frame it may as well be the one we need)
		[frameData, videoInfo] = stLoadMovieFrames(videoFilename, frameIndex, 'a');

		% build previewData data object
		previewData = [];
		previewData.version = 2;
		previewData.thehash = thehash;
		previewData.previewWindowSize = previewWindowSize;
		previewData.videoFilename = videoFilename;
		previewData.videoInfo = videoInfo;
		previewData.videoInfo.PreviewSize = calculatePreviewSize(previewWindowSize, videoInfo);
		previewData.PreviewFrames = cell(1, videoInfo.NumFrames);
		previewData.videoInfo.PreviewFramesAvailable = zeros(1, videoInfo.NumFrames, 'uint8');
		previewData.modified = false;

	end
	
	% upgrade
	if previewData.version == 1
		
		% 14/08/2011 upgraded to version 2, because version 1
		% was storing the preview file on disk filename in fully
		% specified form, and this caused problems when the user
		% moves their temp dir and retains the old preview
		% files. i doubt anyone else has come across this, but i
		% did it, and so i noticed the problem.
		
		% remove this, because the temp dir may be moved by
		% the user, so these fields in the stored files will
		% then be incorrect. we can generate this information
		% when we need it, in any case, so long as we...
		previewData = rmfield(previewData, 'previewFile');
		% ...store this in the previewData object.
		previewData.previewWindowSize = previewWindowSize;
		% upgrade
		previewData.version = 2;
		
	end
	
end

% switch on number of arguments
switch nargin

	case {1 3}

		% validate
		if frameIndex < 1 || frameIndex > length(previewData.PreviewFrames)
			error('frame index out of range');
		end
		
		% do we already have it?
		if ~isempty(previewData.PreviewFrames{frameIndex})
			
			% access it
			previewFrame = previewData.PreviewFrames{frameIndex};
			
			% decode it
			previewFrame = stJpeg(previewFrame);
			
		else
		
			% load a frame from preview object
			if isempty(frameData)
				frameData = stLoadMovieFrames(previewData.videoFilename, frameIndex, 'a');
			end

			% resize it
			previewFrame = imresize(frameData, previewData.videoInfo.PreviewSize);

			% encode it
			previewFrame = stJpeg(previewFrame, 90);
			
			% store it
			if userdata.cache
				previewData.PreviewFrames{frameIndex} = previewFrame;
				previewData.videoInfo.PreviewFramesAvailable(frameIndex) = 1;
				previewData.modified = true;
			end

			% decode it
			previewFrame = stJpeg(previewFrame);
			
		end
		
end

% handle second output argument
if nargout == 2
	videoInfo = previewData.videoInfo;
end







function previewSize = calculatePreviewSize(previewWindowSize, info)

fac = min(previewWindowSize ./ info.Size);
previewSize = round(fac * info.Size);





function h = hash(inp,meth)
% HASH - Convert an input variable into a message digest using any of
%        several common hash algorithms
%
% USAGE: h = hash(inp,'meth')
%
% inp  = input variable, of any of the following classes:
%        char, uint8, logical, double, single, int8, uint8,
%        int16, uint16, int32, uint32, int64, uint64
% h    = hash digest output, in hexadecimal notation
% meth = hash algorithm, which is one of the following:
%        MD2, MD5, SHA-1, SHA-256, SHA-384, or SHA-512 
%
% NOTES: (1) If the input is a string or uint8 variable, it is hashed
%            as usual for a byte stream. Other classes are converted into
%            their byte-stream values. In other words, the hash of the
%            following will be identical:
%                     'abc'
%                     uint8('abc')
%                     char([97 98 99])
%            The hash of the follwing will be different from the above,
%            because class "double" uses eight byte elements:
%                     double('abc')
%                     [97 98 99]
%            You can avoid this issue by making sure that your inputs
%            are strings or uint8 arrays.
%        (2) The name of the hash algorithm may be specified in lowercase
%            and/or without the hyphen, if desired. For example,
%            h=hash('my text to hash','sha256');
%        (3) Carefully tested, but no warranty. Use at your own risk.
%        (4) Michael Kleder, Nov 2005
%
% EXAMPLE:
%
% algs={'MD2','MD5','SHA-1','SHA-256','SHA-384','SHA-512'};
% for n=1:6
%     h=hash('my sample text',algs{n});
%     disp([algs{n} ' (' num2str(length(h)*4) ' bits):'])
%     disp(h)
% end

inp=inp(:);
% convert strings and logicals into uint8 format
if ischar(inp) || islogical(inp)
    inp=uint8(inp);
else % convert everything else into uint8 format without loss of data
    inp=typecast(inp,'uint8');
end

% verify hash method, with some syntactical forgiveness:
meth=upper(meth);
switch meth
    case 'SHA1'
        meth='SHA-1';
    case 'SHA256'
        meth='SHA-256';
    case 'SHA384'
        meth='SHA-384';
    case 'SHA512'
        meth='SHA-512';
    otherwise
end
algs={'MD2','MD5','SHA-1','SHA-256','SHA-384','SHA-512'};
if isempty(strmatch(meth,algs,'exact'))
    error(['Hash algorithm must be ' ...
        'MD2, MD5, SHA-1, SHA-256, SHA-384, or SHA-512']);
end

% create hash
x=java.security.MessageDigest.getInstance(meth);
x.update(inp);
h=typecast(x.digest,'uint8');
h=dec2hex(h)';
if(size(h,1))==1 % remote possibility: all hash bytes < 128, so pad:
    h=[repmat('0',[1 size(h,2)]);h];
end
h=lower(h(:)');
clear x




function userdata = getUserData

% get mode
switch stUserData('previewPolicy')
	case 'On disk'
		userdata.persist = true;
		userdata.cache = true;
	case 'In memory'
		userdata.persist = false;
		userdata.cache = true;
	case 'Never'
		userdata.persist = false;
		userdata.cache = false;
	otherwise
		error(['Unrecognised preview policy "' stUserData('previewPolicy') '"']);
end

% prepare
userdata.tempdir = stUserData('temporaryDirectory');

% create preview dir
if isempty(userdata.tempdir)
	userdata.previewdir = [];
else
	userdata.previewdir = [userdata.tempdir '/preview'];
	if ~exist(userdata.previewdir, 'dir')
		mkdir(userdata.previewdir);
	end
end





function previewFilename = getPreviewFilename(userdata, previewWindowSize, thehash)

if isempty(userdata.previewdir)
	% temp dir not set by user, return []
	previewFilename = '';
end

% create filename
previewPath = [userdata.previewdir '/' sprintf('%ix%i', previewWindowSize)];
previewFilename = [previewPath '/' thehash '.mat'];

% make sure sub-path exists
if ~exist(previewPath, 'dir')
	mkdir(previewPath);
end


