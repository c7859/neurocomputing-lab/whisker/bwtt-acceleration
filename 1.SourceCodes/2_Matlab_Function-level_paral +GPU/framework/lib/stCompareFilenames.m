
% i = stCompareFilenames(a, b)
%   stCompareFilenames returns true if the two filenames are
%   functionally equivalent (i.e. specify the same file or
%   folder).


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function i = stCompareFilenames(a, b)

a = stTidyFilename(a);
b = stTidyFilename(b);

if ispc
	i = strcmpi(a, b);
else
	i = strcmp(a, b);
end

end


