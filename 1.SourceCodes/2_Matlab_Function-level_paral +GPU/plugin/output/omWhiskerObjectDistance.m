
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function state = omWhiskerObjectDistance(operation, job, state)

switch operation
	
	case 'info'
		
		state.author = 'Ben Mitch';

		
		
	case 'parameters'
		
		pars = {};
		
		% parameter
		par = [];
		par.name = 'resolution';
		par.label = 'Object Resolution (pixels)';
		par.help = 'Lower values will provide more accurate answers, but will take longer to compute.';
		par.value = 5;
		par.type = 'scalar';
		par.range = [1 5];
		par.step = [1 1];
		par.precision = 0;
		pars{end+1} = par;
		
		state.pars = pars;
	

		
	case 'process'

		% empty
		raw = cell(1, length(state.frameIndices));
		minimum = NaN(length(state.frameIndices), 1);
		
		% extract
		results = job.getResults('all');
% 		mmPerPixel = job.getMetaData('mmPerPixel');
		anyResults = false;
		
		% for each frame
		for f = 1:length(state.frameIndices)
			
			% extract
			fi = state.frameIndices(f);
	 		result = results{fi};
			
			% if we have everything we need
			if isfield(result, 'object') && isfield(result, 'whisker')
				
				% prep
				D = [];
				
				% extract
				O = result.object.models;
				W = result.whisker.contour;
				
				% for each object
				for oi = 1:length(O)
					
					% extract
					o = O{oi};
					
					% convert to object contour
					o = stObjectContourFromModel(o, state.pars.resolution);
				
					% for each whisker
					for wi = 1:length(W)

						% extract
						w = W{wi};
						
						% get inter-pixel distance for each pixel pair
						dd = sqrt(sepsq_gg(o, w));
						D(wi, oi) = min(min(dd));
						
					end

				end
				
				% store
				anyResults = true;
				raw{f} = D;
				md = min(D);
				minimum(f, 1:length(md)) = md;
				
			end
		
		end	
		
		% store
		if anyResults
			state.result.raw = raw;
			state.result.min = minimum;

% 			% convert
% 			if ~isempty(mmPerPixel)
% 				state.result.mm{f} = state.result.pixel{f} * mmPerPixel;
% 			end
			
		else
			state.result = [];
		end
		


		
	case 'plot'

		% create axes
		state.h_axis = gca;
		
		% do plot
		plot(state.h_axis, state.frameIndices, state.result.min);
		ylabel('whisker-object distance (pixels)');
		xlabel('frame index');
		
		% do legend
		n = size(state.result.min, 2);
		ll = {};
		for i = 1:n
			ll{i} = ['Object ' int2str(i)];
		end
		legend(ll);


		
		
	otherwise
		
		state = [];
		
end




function C = sepsq_gg(A, B)

% This implementation due to Germano Gomes at Matlab Central

C = bsxfun(@plus,dot(A,A)',dot(B,B)) - 2*A'*B;




