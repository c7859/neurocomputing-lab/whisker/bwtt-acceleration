
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [newConnectivityMatrix, whiskerTreeLUTwithOrientations] = extractSpatialConnectivityWithTensorVoting(whiskerTreeLUT, angleIm, whiskerTree, roundness, saliency, flagFigure1On, curveDifference)
% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Function extractSpatialConnectivityWithTensorVoting
% Inputs:   connectivityMatrix has the format [curveIn NodeIn curveOut
%           nodeOut distance]
%           whiskerTree structure is the structure of tokens with the
%               following format [x,y,intensity,theta, rho] (average values
%               for each token
%           roundnessImage contains the encoding of the second eigenvalue
%           describing the roundnes of the image
%           saliencyImage encodes the saliency of the feature indicating
%           how strong is the local orientation
% Outputs:  newConnectivityMatrix [curveIn NodeIn curveOut
%           nodeOut distance]
%           -whiskerTreeLUTwithOrientations is a matrix, where the data are
%           [inCurve inMarker ::::: xofmarker, yofmarker, c, theta,
%           rho,angle, roundness, saliency]
%
% For each node in whiskerTreeLUT find the elements within oneicurveDifference and
% check if the difference of the angle is under Tangle
%<\outDOC>
%%
if nargin < 7,
    curveDifference = 1;
end

L = (4*curveDifference)^2;
Tsmooth = 30;

newConnectivityMatrix = zeros(5000,5);
newConnectivityMatrixIndex = 0;



whiskerTreeLUTwithOrientations = zeros(size(whiskerTreeLUT,1),10);
S = size(angleIm);
whiskerTreeLUT(round(whiskerTreeLUT(:,3)) == 1,3) = 2;
whiskerTreeLUT(round(whiskerTreeLUT(:,4)) == 1,4) = 2;
whiskerTreeLUT(round(whiskerTreeLUT(:,3)) == S(1),3) = S(1)-1;
whiskerTreeLUT(round(whiskerTreeLUT(:,4)) == S(2),4) = S(2)-1;

for i=1:size(whiskerTreeLUT,1)
    x1 = round(whiskerTreeLUT(i,3));
    y1 = round(whiskerTreeLUT(i,4)); 
    angleN1 = angleIm(x1+[-1:1], y1+[-1:1]);
    %angle1m = mean(angleN1(angleN1 ~= -inf));
    angle1m = angleIm(x1, y1);
    if angle1m == -inf
        angle1 = mean(angleN1(angleN1 ~= -inf));
        roundness1 = 5; % avoid direction interpretation
    else
        angle1v = std(angleN1(angleN1 ~= -inf));
        angle1 = mean(angleN1(abs(angleN1 - angle1m) <= min(angle1v,20)));
        roundness1 = roundness(x1,y1);
    end
    saliency1 = saliency(x1,y1);

    whiskerTreeLUTwithOrientations(i,:) = [whiskerTreeLUT(i,:), angle1, roundness1, saliency1];
end


% consider iCurves between i and i+curveDifference
for i=1:max(whiskerTreeLUT(:,1))-curveDifference,
    idx = (whiskerTreeLUT(:,1) >= i) & (whiskerTreeLUT(:,1) <= i+curveDifference);
    candidatePoints = whiskerTreeLUTwithOrientations(idx,:);
    if ~isempty(candidatePoints),
        for j=1:sum(candidatePoints(:,1) == i),
            % for each element, extract its x,y, find neighbouring elements
            % under L and connect them if the angle criteria match
            x1 = candidatePoints(j,3);
            y1 = candidatePoints(j,4);
            inCurve = candidatePoints(j,1);
            inMarker =  candidatePoints(j,2);
            distanceBetweenPoints = ( x1 - candidatePoints(:,3)).^2 +...
                (y1 - candidatePoints(:,4)).^2;
            idx = (distanceBetweenPoints > 0) & (distanceBetweenPoints < L);
            selectedPoints = candidatePoints(idx,:);

            angle1 = candidatePoints(j,8);

            for t = 1:size(selectedPoints,1),
                x2 = (selectedPoints(t,3));
                y2 = (selectedPoints(t,4));

                angle2 = selectedPoints(t,8);
                tdeg = atand((y1-y2)/(x1-x2));
                if tdeg < 0, tdeg = 180+tdeg; end

                if ((abs(tdeg - angle1) < Tsmooth) || ((180 - abs(tdeg - angle1)) < Tsmooth)) &&...
                        ((abs(tdeg - angle2) < Tsmooth) || ((180 - abs(tdeg - angle2)) < Tsmooth))
                    %if (abs(tdeg - angle1) < Tsmooth) && (abs(tdeg - angle2) < Tsmooth)
                    % create a new entry in the map
                    outCurve = selectedPoints(t,1);
                    outMarker = selectedPoints(t,2);
                    newConnectivityMatrixIndex = newConnectivityMatrixIndex + 1;
                    newConnectivityMatrix(newConnectivityMatrixIndex,:) = [inCurve inMarker outCurve outMarker 1];
                end
            end
        end
    end
end

newConnectivityMatrix = newConnectivityMatrix(1:newConnectivityMatrixIndex,:);
% make symmetric
newConnectivityMatrix = sortrows([newConnectivityMatrix; newConnectivityMatrix(:,3:4), newConnectivityMatrix(:,[1 2 5])]);


if flagFigure1On
figure(1)
hold on
for i=1:size(newConnectivityMatrix,1),
        inCurve = newConnectivityMatrix(i,1);
        inMarker = newConnectivityMatrix(i,2);
        outCurve = newConnectivityMatrix(i,3);
        outMarker = newConnectivityMatrix(i,4);
        distOfEl = newConnectivityMatrix(i,5);
        x1 = whiskerTree{inCurve}(inMarker,1);
        y1 = whiskerTree{inCurve}(inMarker,2);
        x2 = whiskerTree{outCurve}(outMarker,1);
        y2 = whiskerTree{outCurve}(outMarker,2);
        line([y1 y2],[x1 x2],'Color',[1 0.5 0],'linewidth',1);%cmap(distOfEl,:));

        %line([y1 y2],[x1 x2],'Color', [0 1 0], 'LineWidth', 2);
end
end
