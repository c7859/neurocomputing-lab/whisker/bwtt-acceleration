
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function T = circularField(sigma)


    S = ceil(sqrt(-log(0.01)*sigma^2)*2);
    S = floor(S/2)*2+1;
    
    T = zeros(S,S,2,2);
    for theta = (0:1/32:1-1/32)*2*pi
        v = [cos(theta);sin(theta)];
        B = stickField(v,sigma);
        T = T + B;
    end
    T = T/32;
end