
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



% author ben mitch
% citation not required



function output = ppDespeckle(operation, job, input)

% a "speckle" is defined as a pixel that has a completely
% different luminance value from all (8) neighbours. this
% can occur if the source CCD had incorrectly calibrated or
% dead pixels. despeckle identifies these pixels, and sets
% them equal to the average of their neighbours.
%
% author: mitch
% created: 10/05/11



switch operation
	
	case 'info'
		
		output.author = 'Ben Mitch';
		
		
		
	case 'parameters'
		
		% empty
		pars = {};
		
		% parameter
		par = [];
		par.name = 'thresh';
		par.label = 'Threshold (%)';
		par.range = [0 50];
		par.step = [1 5];
		par.value = 5;
		par.type = 'scalar';
		pars{end+1} = par;
		
		% ok
		output.pars = pars;
		
		
		
	case 'initialize'
		
		% retrieve parameters
		output.pars = job.getRuntimeParameters();
		
		% process % into luminance level
		output.pars.thresh = uint8(output.pars.thresh / 100 * 255);

		
	
	case 'process'
		
		% get frame
		frame = job.getRuntimeFrame(input.frameIndex);
		
		% process
		frame{1} = despeckle(frame{1}, input.pars.thresh);
		
		% set frame
		job.setRuntimeFrame(input.frameIndex, frame);
		
		% propagate
		output = input;
		
		
		
	otherwise
		output = [];
		
		
end











function im = despeckle(im, thresh)

% a "speckle" is defined as a pixel that has a completely
% different luminance value from all (8) neighbours. this
% can occur if the source CCD had incorrectly calibrated or
% dead pixels. despeckle identifies these pixels, and sets
% them equal to the average of their neighbours.

speckle = logical(im == -1);

% define neighbours
R = [-1 -1 -1 0 0 1 1 1];
C = [-1 0 1 -1 1 -1 0 1];

% define working range
sz = size(im);
rngR = 2:sz(1)-1;
rngC = 2:sz(2)-1;

% get min/max
mn = 255 * im(rngR, rngC);
mx = 0 * im(rngR, rngC);

% get range for neighbours of each pixel
for n = 1:8

	r = R(n);
	c = C(n);
	neighbour = im(rngR-r, rngC-c);
	mn = min(mn, neighbour);
	mx = max(mx, neighbour);
	
end

% identify pixels that exceed a range around their
% neighbours (i.e. that look like delta functions)
above = (im(rngR, rngC) - mx) >= thresh;
below = (mn - im(rngR, rngC)) >= thresh;
speckle(rngR, rngC) = above | below;
f = find(speckle);

% reset these pixels to the average of their neighbours
if ~isempty(f)
	R = sz(1);
	n = [f-R-1 f-R f-R+1 f-1 f+1 f+R-1 f+R f+R+1];
	im(f) = uint8(mean(double(im(n)), 2));
end



