
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function state = wtIgorProcess(job, state)

%*************************************************************************
%  Author: Igor Perkon   -    perkon@sissa.it
%***********************************************

%% called as
%state.pars.iFrame = iFrame;
%[objects, newObjects, measures, assignedObjects, old] = ...
% wtIgor(allwhiskers, allOldWhiskers, snoutTrackingData, ...
% oldSnoutTrackingData, GRAYshape, objects, state.pars, old, flagPlot);
%
%% with this IP
%function [objects, newObjects, measures, assignedObjects, old] =...
%    wtIgor(allwhiskers, allOldWhiskers, snoutTrackingData,...
% oldSnoutTrackingData, GRAYshape, objects, state.pars, old, flagPlot, trackingData)



%% RETRIEVE STATE

% get frame index
frameIndex = state.frameIndex;

% MITCH
% % may as well alert user if we fail on no preproc, since it
% % takes a long time to compute this in the error condition
% if ~job.isPreprocPresent(frameIndex)
%     error('cannot compute wtIgor - no pre-processing is configured (recommend ppBgExtractionAndFilter)');
% end
% MITCH

% get snoutTrackingData
results = job.getResults(frameIndex);

if ~isfield(results{1}, 'snout')
    error('cannot compute wtIgor - no snout tracking available');
end
snoutTrackingData = results{1}.snout;


% get snoutTrackingData
oldresults = job.getResults(frameIndex-1);

% MITCH
if ~isfield(oldresults{1}, 'snout') || ~isfield(oldresults{1}, 'whisker')
% MITCH

	% do nothing;
else
    oldSnoutTrackingData = oldresults{1}.snout;
    
    
    state.pars.startId = state.old.startId;
    
    % 1. process snout Tracking data
    orientationDifference = snoutTrackingData.orientation - oldSnoutTrackingData.orientation;
    
    state.pars.snoutCenter = snoutTrackingData.center;
    state.pars.snoutOrientationDifference = orientationDifference;
    
    old = state.old;
    objects = state.objects;
    
    % 2. update object motion with prediction
    uobjects = updateObjects(objects,0,state.pars);
    
    %% **********************************************************************
    % regenerate new whiskers using history
    %% ***********************************************************************
    if 0,
        % disabled beacause not available in ST
        flagFigure1On = 0;
        whiskers = detectWhiskersFromObjects(trackingData, snoutTrackingData, allwhiskers, uobjects, flagFigure1On);
        allwhiskers = whiskers;
        old.whiskers = whiskers;
    end
    % 3. get whiskers and create measures
    % old frame
    
    for i=1:numel(oldresults{1}.whisker.contour),
        oldresults{1}.whisker.contour{i} =fliplr(oldresults{1}.whisker.contour{i}');
    end

    
    allOldWhiskers = filterMeasurements(oldresults{1}.whisker.contour, oldSnoutTrackingData, ...
        state.pars.minL, state.pars.maxL, state.pars.minR, state.pars.maxR);
    
    oldmeasures = createMeasuresFromWhiskers(allOldWhiskers, oldSnoutTrackingData);
    %% current frame
    
    for i=1:numel(results{1}.whisker.contour),
        results{1}.whisker.contour{i} = fliplr(results{1}.whisker.contour{i}');
    end
    allwhiskers = filterMeasurements(results{1}.whisker.contour, snoutTrackingData, ...
        state.pars.minL, state.pars.maxL,state.pars.minR, state.pars.maxR);
    
    measures = createMeasuresFromWhiskers( allwhiskers, snoutTrackingData, state.pars.approxError);
    
 
    
    %%
    %
    % if flagPlot
    %     for i=1:numel(uobjects)
    %         if uobjects{i}.flagNotAssigned == 0
    %             plotData(uobjects{i}.state, snoutTrackingData, [0.2 0.8 0.2],1);
    %         else
    %             plotData(uobjects{i}.state, snoutTrackingData, [0 1 0],1);
    %         end
    %         i,pause
    %     end
    % end
    %%
    
    % 4. create cost matrix between measures and objects
    costMatrixObjects = createCostMatrix(measures, uobjects, 'autoT', state.pars, 1);
    
    state.pars.costMatrixObjects = costMatrixObjects;
    state.pars.meas = measures;
    
    assignedObjectsBU = manageTracking(uobjects, costMatrixObjects,  'k&w', state.pars);
    
    % 5. assign measures to objects and update them
    [updatedObjects, assignedObjects] = assignMeasurementAndUpdateObjects(uobjects, measures,...
        assignedObjectsBU, 'keepLength', state.pars);
    %%
    if 0
        figure(1)
        figure(1)
        imshow(GRAYshape)
        colormap(gray);
        hold on;
        for i=1:numel(updatedObjects)
            if updatedObjects{i}.flagNotAssigned == 0
                %plotData(measures{assignedObjects(i)}, snoutTrackingData, [1 0 0]);
                plotData(updatedObjects{i}.state, snoutTrackingData, updatedObjects{i}.color,1);
            else
                plotData(updatedObjects{i}.state, snoutTrackingData, [0 1 0],1.5);
            end
        end
    end
    
    %% 6. create cost matrix between current and old measures
    [costMatrix, additionalOutput] = createCostMatrix(oldmeasures, measures, '3d', state.pars, 1);
    
    if ~isfield(state.pars,'exclusiveInitializeCost')
        state.pars.exclusiveInitializeCost = 0;
    end
    %%
    state.pars.intraFrameCostMatrix = createCostMatrix(measures, uobjects, '3d', state.pars, 1);%costMatrixObjects;
    [currentStateIdx, previousStateIdx] = manageMeasurements...
        (assignedObjects, costMatrix, ...
        'exclusivewithmemory', state.pars, old);
    %%
    
    if 0
        figure(2)
        %% imshow(GRAYshape)
        colormap(gray);
        hold on;
        for i=1:numel(measures)
            plotData(measures{i}, snoutTrackingData, [1 1 1]);
        end
        for i=previousStateIdx
            plotData(oldmeasures{i}, snoutTrackingData, [1 0 1]);
        end
        
        for i=currentStateIdx
            plotData(measures{i}, snoutTrackingData, [0.5 1 0]);
        end
        
        hold off
    end
    
    %% filter short objects under 35 elements
    idx = zeros(1,numel(currentStateIdx));
    for i=1:numel(currentStateIdx),
        c = measures{currentStateIdx(i)}.knots;
        lgt1 = c(end)-c(1);
        c = oldmeasures{previousStateIdx(i)}.knots;
        lgt2 = c(end)-c(1);
        if min(lgt1,lgt2) > 35,
            idx(i) = 1;
        end
    end
    idx = logical(idx);
    currentStateIdx = currentStateIdx(idx);
    previousStateIdx = previousStateIdx(idx);
    
    
    newObjects = createObject(measures(currentStateIdx), oldmeasures(previousStateIdx),...
        'basic', state.pars, 0);
    state.pars.startId = state.pars.startId + numel(currentStateIdx);
    %%
    flagNA = 0;
    if 0
        figure(mod(state.pars.iFrame,2)+3)
        %     if mod(state.pars.iFrame, 10) == 1,
        %         figure(state.pars.iFrame)
        %     end
        hold off
        imshow(GRAYshape)
        colormap(gray);
        hold on;
        
        for i=1:numel(updatedObjects),
            % plot all with their colors
            if updatedObjects{i}.flagIsNew
                plotData(updatedObjects{i}.state, snoutTrackingData, [1 1 1],2);
            elseif updatedObjects{i}.OcclusionTime < 10,
                plotData(updatedObjects{i}.state, snoutTrackingData, updatedObjects{i}.color,2);
            elseif updatedObjects{i}.framesNotAssigned < 4,
                plotData(updatedObjects{i}.state, snoutTrackingData,updatedObjects{i}.color,3);
            else
                plotData(updatedObjects{i}.state, snoutTrackingData, updatedObjects{i}.color,1);
            end
            %objects{i}.Id, pause;
        end
        
        %     for j=1:numel(measures)
        %         if sum(j == assignedObjects)==0
        %             % plot unassigned measures
        %             plotData(measures{j}, snoutTrackingData, [1 0 0],1.5);
        %         end
        %         %         else
        %         %           plotData(measures{j}, snoutTrackingData, [1 0 0],1);
        %         %         end
        %         %         j,pause
        %         %          plotData(measures{j}, snoutTrackingData, [1 1 0],1);
        %         %          pause
        %     end
        %
        %
        %     for i=1:numel(updatedObjects),
        %         if updatedObjects{i}.flagNotAssigned
        %             plotData(updatedObjects{i}.state, snoutTrackingData, updatedObjects{i}.color,2);
        %             flagNA = flagNA+1;
        %         else
        %             plotData(updatedObjects{i}.state, snoutTrackingData, updatedObjects{i}.color,1);
        %         end
        %         % i,pause
        %     end
        
        
        for i=1:numel(newObjects),
            plotData(newObjects{i}.state, snoutTrackingData, [1 1 1],2);
        end
        hold off;
    end
    %%

    
    objects = [updatedObjects, newObjects];
    
    trackedW = cell(1,numel(objects));
    for i=1:numel(objects),
        trackedW{i}.xy = plotData(objects{i}.state, snoutTrackingData, [1 0 0],0);
        trackedW{i}.Id = objects{i}.Id;
        trackedW{i}.color = objects{i}.color;
    end
            
    old.a = [assignedObjects, currentStateIdx];
    old.b = currentStateIdx;
    old.startId = state.pars.startId;
    
    state.objects = objects;
    
    % store results

		%STORE%
		whisker = results{1}.whisker; % get existing
		whisker.tracking = trackedW; % augment
    job.setResults(frameIndex, 'whisker', whisker);
		%STORE%
		
%     results.objects = objects;
%     results.snoutTrackingData = snoutTrackingData;
%     job.setResults(frameIndex, 'whisker', results);
end
