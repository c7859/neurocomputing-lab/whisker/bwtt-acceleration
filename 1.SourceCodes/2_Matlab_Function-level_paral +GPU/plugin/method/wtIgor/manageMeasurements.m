
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [currentStateIdx, previousStateIdx] = manageMeasurements...
    (assignedObjects, costMatrix, method, param, oldAssignedObjects)

%*************************************************************************
%  Author: Igor Perkon   -    perkon@sissa.it
%***********************************************

% <\inDOC>
% Inputs:
%       - costMatrix is the set of similarity matrices
%       - objects is a cell array of cubic splines in polar coordinates
%       - method defines the method used for calculatinf
%       - param is the structure of paramters
%       - flagPlot is a flag used for selecting plot of one or more
%      whiskers
idx = (assignedObjects == 0);
assignedObjects = assignedObjects(~idx);
assignement = 1:size(costMatrix{1},2);
assignement(assignedObjects) = nan; % exlcude measurements already assigned
idx = isnan(assignement);
assignement = assignement(~idx);

currentStateIdx = nan(size(assignement));
previousStateIdx = nan(size(assignement));
if isempty(oldAssignedObjects.a)
    oldAssignedObjects.a = 0;
end

switch (method)

    case 'exclusivewithmemory'
        % assigns the best match if under distT
        % and if second element is at least over minShapeDistT
        %
        % for every object (i.e. for every column)
        % a meaasurment can be assigned only to one object
        % if an other object is claiming the same is discarded
        % the 'withmemory' options requires that in two frames
        % the measurements are not assigned
        [bestCost, pos] = min(abs(costMatrix{1}));
        j = 0;
        for i=assignement
            j = j + 1;
            if bestCost(i) < param.startCostT,
                % check if there are other candidates with the same measure
                idx = (pos == pos(i));
                if sum(idx) == 1,
                    % check if used
                    if min(oldAssignedObjects.a ~= pos(i))
                        val = min(param.intraFrameCostMatrix{1}(i,:));
                        if isempty(val),
                            previousStateIdx(j) = pos(i);
                        elseif val(1) > param.exclusiveInitializeCost,
                            previousStateIdx(j) = pos(i);
                        end
                    end
                elseif bestCost(i) == min(bestCost(idx))
                    % if the object was previously not assigned
                    if sum(oldAssignedObjects.a == pos(i))==0
                        % check if there are no
                        % whiskers close one to an
                        % other
                        val = min(param.intraFrameCostMatrix{1}(i,:));
                        if isempty(val),
                            previousStateIdx(j) = pos(i);
                        elseif val(1) > param.exclusiveInitializeCost,
                            previousStateIdx(j) = pos(i);
                        end
                    end
                end
            end
        end
        idx = ~isnan(previousStateIdx);
        currentStateIdx = assignement(idx);
        previousStateIdx = previousStateIdx(idx);

    case 'basic'
        % assigns the best match if under distT
        % for every object (i.e. for every column)
        % a meaasurment can be assigned only to one object
        % if an other object is claiming the same is discard
        [bestCost, pos] = min(abs(costMatrix{1}));
        j = 0;
        for i=assignement
            j = j + 1;
            if bestCost(i) < param.startCostT,
                % check if there are other candidates with the same measure
                idx = (pos == pos(i));
                if sum(idx) == 1,
                    previousStateIdx(j) = pos(i);
                elseif bestCost(i) == min(bestCost(idx))
                    previousStateIdx(j) = pos(i);
                end
            end
        end
        idx = ~isnan(previousStateIdx);
        currentStateIdx = assignement(idx);
        previousStateIdx = previousStateIdx(idx);


    case 'withmemory'
        % assigns the best match if under distT
        % for every object (i.e. for every column)
        % a meaasurment can be assigned only to one object
        % if an other object is claiming the same is discarded
        % the 'withmemory' options requires that in two frames
        % the measurements are not assigned
        [bestCost, pos] = min(abs(costMatrix{1}));
        j = 0;
        for i=assignement
            j = j + 1;
            if bestCost(i) < param.startCostT,
                % check if there are other candidates with the same measure
                idx = (pos == pos(i));
                if sum(idx) == 1,
                    % check if used
                    if min(oldAssignedObjects.a ~= pos(i))
                        previousStateIdx(j) = pos(i);
                    end
                elseif bestCost(i) == min(bestCost(idx))
                    % if the object was previously not assigned
                    if sum(oldAssignedObjects.a == pos(i))==0
                        previousStateIdx(j) = pos(i);
                    end
                end
            end
        end
        idx = ~isnan(previousStateIdx);
        currentStateIdx = assignement(idx);
        previousStateIdx = previousStateIdx(idx);
end



