
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [W, Q0, UR, bbAvg, Xavg, H] = getSplineSpaceParameters(bb)
% Author: Perkon Igor    -    perkon@sissa.it
%
%<\inDOC>
% W - is the coefficient to shape space matrix
% Q0 - is the defualt centered template c
% UR -is the regulariser matrix
%<\outDOC>

try
    if strcmp(bb.form,'pp')
        bb = fn2fm(bb,'B-');
    end
catch err
    if isempty(which('fn2fm'))
        error(['Cannot use stShapeSpaceKalman unless Spline Toolbox is installed (function "fn2fm" was not detected on path)']);
    end
    rethrow(err)
end

bbAvg = bb;
Qx = bb.coefs(1,:);
QavgX = Qx - mean(Qx);
Qy = bb.coefs(2,:);
QavgY = Qy - mean(Qy);
bbAvg.coefs = [QavgX; QavgY];
Q0 = [QavgX'; QavgY'];



% W is defined on p.74
W = [ones(size(Qx))', zeros(size(Qy))', QavgX', -QavgY';...
     zeros(size(Qx))', ones(size(Qy))', QavgY', QavgX'];
 
Xavg = [mean(Qx), mean(Qy), 0 0]';



%% 6. find the matrix UR as integral of the matrix U
UR = zeros(2*numel(Qx));

Basis = spmak(bbAvg.knots, eye(numel(Qx)));
dL = 0.1;
L = bbAvg.knots(end);

for l=0:dL:L,
     % U is defined on p. 58 as 2x2Nq matrix
     U = [fnval(Basis, l)' zeros(size(Qx));  zeros(size(Qx)) ,fnval(Basis, l)'];
     % UR is defined on p. 58 as a 2Nqx2Nq matrix
     UR = UR + dL/L* (U' * U);
end

%% 7. find the pseudoinverse W+ alias Wp a as p. 79

% H is also defined on p. 79, 4*4
H = W'*UR*W;

% Wp is an error minimising projection onto shape-space, size 4*2Nx
Wp = inv(H)*W'*UR;
