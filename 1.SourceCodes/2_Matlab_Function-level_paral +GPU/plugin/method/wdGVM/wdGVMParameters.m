
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====




function pars = wdGVMParameters()




pars = {};


% parameter
par = [];
par.name = 'erodeRadius';
par.type = 'scalar';
par.label = 'Erode radius';
par.range = [2 10];
par.value = 6;
par.step = [1 2];
pars{end+1} = par;

% parameter
par = [];
par.name = 'radInv';
par.type = 'scalar';
par.label = 'Radius of inversion';
par.range = [50 250];
par.value = 160;
par.step = [1 10];
pars{end+1} = par;

% parameter
par = [];
par.name = 'hhThreshold';
par.type = 'scalar';
par.label = 'HH Threshold';
par.range = [50 250];
par.value = 160;
par.step = [1 10];
pars{end+1} = par;

% parameter
par = [];
par.name = 'erodeBtoW';
par.type = 'scalar';
par.label = 'Erode black to white';
par.range = [2 20];
par.value = 10;
par.step = [0.5 2];
par.precision = 1;
pars{end+1} = par;

% parameter
par = [];
par.name = 'edgeDetectLevelNum';
par.type = 'scalar';
par.label = 'Edge Detect Level';
par.range = [1 80];
par.value = 32;
par.step = [1 5];
pars{end+1} = par;











