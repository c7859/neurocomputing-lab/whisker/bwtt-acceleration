
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



% FAIR USE
%
% Co-authorship requirement. This method has not yet been
% published and you should offer co-authorship of the first
% publication making use of it. See the URL below for more
% information:
%
% http://bwtt.sourceforge.net/fairuse



function output = wtIgor(operation, job, input)

switch operation
    
    
    case 'info'
        
        % specify author
        output.author = 'Igor Perkon';
        output.flags = {'noplot'};
				output.citation = 'co-authorship';
        
    case 'parameters'
        
        pars = loadwtIgorParameters();
        % ok
        output.pars = pars;
        
    case 'initialize'
        
        % retrieve parameters
        output.pars = job.getRuntimeParameters();
        
        
        output.objects = [];
        output.old.a = [];
        output.old.startId = 0;
        
    case 'process'
        
        output = wtIgorProcess(job, input);
        
    
        
    case 'plot'
        output.h = [];
        results = job.getResults(input.frameIndex);
        
        %axes(input.h_axis);
        % get the image
        %GRAYshape = job.getRuntimeFrame(frameIndex);
        
        if isfield(results{1}, 'trackedObjects')
            %% hold off
            %imshow(GRAYshape)
            %colormap(gray);
            
            hold on;
            snoutTrackingData = results{1}.trackedObjects.snoutTrackingData;
            objects = results{1}.trackedObjects.objects;
            
            % MITCH
            %             h = cell(1,numel(objects));
            h = [];
            %trackedObjects = cell(1,numel(objects));
            axes(input.h_axis)
            for i=1:numel(objects),
                % plot all with their colors
                if objects{i}.flagIsNew
                    [pts, h(end+1)] = plotData(objects{i}.state, snoutTrackingData, [1 1 1],2);
                elseif objects{i}.OcclusionTime < 10,
                    [pts, h(end+1)] = plotData(objects{i}.state, snoutTrackingData, objects{i}.color,2);
                elseif objects{i}.framesNotAssigned < 4,
                    [pts, h(end+1)] = plotData(objects{i}.state, snoutTrackingData,objects{i}.color,3);
                else
                    [pts, h(end+1)] = plotData(objects{i}.state, snoutTrackingData, objects{i}.color,1);
                end
                %trackedObjects{i} = pts;
            end
            % MITCH
            
            output.h = h;
            %output.trackedObjects = trackedObjects;
        end
%         hold off
        
        
    otherwise
        output = [];
end