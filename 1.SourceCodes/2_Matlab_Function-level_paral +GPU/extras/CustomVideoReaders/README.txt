
stLoadMovieFrames_EhudFonioBinary.m
stLoadMovieFrames_AtlasACopy.m

stLoadMovieFrames() has had all custom code removed from it. Users who need custom behaviour can now provide that behaviour by providing their own implementation of stLoadMovieFrames.m. Included in this folder are two implementations; one is for Ehud Fonio binary files (this may need some modification, I've done my best to respect the original code I saw), the other is for Atlas Group A copy files.

To install these files, copy them into the folder where the videos themselves are stored, and remove the underscore and suffix to leave the filename as "stLoadMovieFrames.m". If stLoadMovieFrames() finds a file named thus in the same folder as a video file it has been asked to load, it will delegate to that locally stored file. Thus, you will need to store a copy of this file alongside all videos that need a custom loader. If this is impractical, get in touch with me to discuss other solutions (there are other ways to achieve this).

Once installed, you can test the file by calling it thus:

[frames, movieInfo] = stLoadMovieFrames(videoFilename, [11 12 13]);

NB: The Atlas Group file should not need testing, it has already been tested. The Ehud Fonio file does need testing, since I did not have access to any of the source video files.
