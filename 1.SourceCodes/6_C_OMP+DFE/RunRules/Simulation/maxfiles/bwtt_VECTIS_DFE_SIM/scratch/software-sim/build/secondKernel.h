#ifndef SECONDKERNEL_H_
#define SECONDKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class secondKernel : public KernelManagerBlockSync {
public:
  secondKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_px;
  t_port_number m_bx;
  t_port_number m_py;
  t_port_number m_by;
  t_port_number m_siRho;
  t_port_number m_soRho;
  t_port_number m_output;
  HWOffsetFix<1,0,UNSIGNED> id51out_io_output_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id82out_value;

  HWOffsetFix<32,0,UNSIGNED> id0out_tokensIndex;

  HWOffsetFix<32,0,UNSIGNED> id2out_count;
  HWOffsetFix<1,0,UNSIGNED> id2out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id2st_count;

  HWOffsetFix<32,0,UNSIGNED> id81out_value;

  HWOffsetFix<1,0,UNSIGNED> id4out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id5out_io_px_force_disabled;

  HWFloat<8,24> id8out_data;

  HWOffsetFix<1,0,UNSIGNED> id8st_read_next_cycle;
  HWFloat<8,24> id8st_last_read_value;

  HWOffsetFix<10,0,UNSIGNED> id70out_output[5];

  HWFloat<8,24> id57out_dataa[3];

  HWFloat<8,24> id32out_result[9];

  HWOffsetFix<1,0,UNSIGNED> id71out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id9out_io_bx_force_disabled;

  HWFloat<8,24> id12out_data;

  HWOffsetFix<1,0,UNSIGNED> id12st_read_next_cycle;
  HWFloat<8,24> id12st_last_read_value;

  HWFloat<8,24> id33out_result[13];

  HWOffsetFix<10,0,UNSIGNED> id77out_output[15];

  HWOffsetFix<32,0,TWOSCOMPLEMENT> id55out_dataa[3];

  HWFloat<8,24> id30out_o[7];

  HWFloat<8,24> id34out_result[13];

  HWFloat<8,24> id38out_result[9];

  HWOffsetFix<1,0,UNSIGNED> id13out_io_py_force_disabled;

  HWFloat<8,24> id16out_data;

  HWOffsetFix<1,0,UNSIGNED> id16st_read_next_cycle;
  HWFloat<8,24> id16st_last_read_value;

  HWFloat<8,24> id35out_result[9];

  HWOffsetFix<1,0,UNSIGNED> id17out_io_by_force_disabled;

  HWFloat<8,24> id20out_data;

  HWOffsetFix<1,0,UNSIGNED> id20st_read_next_cycle;
  HWFloat<8,24> id20st_last_read_value;

  HWFloat<8,24> id36out_result[13];

  HWOffsetFix<32,0,TWOSCOMPLEMENT> id56out_dataa[3];

  HWFloat<8,24> id31out_o[7];

  HWFloat<8,24> id37out_result[13];

  HWFloat<8,24> id39out_result[9];

  HWFloat<8,24> id40out_result[13];

  HWFloat<8,24> id80out_value;

  HWOffsetFix<1,0,UNSIGNED> id42out_result[3];

  HWOffsetFix<1,0,UNSIGNED> id21out_io_siRho_force_disabled;

  HWFloat<8,24> id24out_data;

  HWOffsetFix<1,0,UNSIGNED> id24st_read_next_cycle;
  HWFloat<8,24> id24st_last_read_value;

  HWOffsetFix<1,0,UNSIGNED> id43out_result[3];

  HWOffsetFix<1,0,UNSIGNED> id75out_output[53];

  HWOffsetFix<1,0,UNSIGNED> id44out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id25out_io_soRho_force_disabled;

  HWFloat<8,24> id28out_data;

  HWOffsetFix<1,0,UNSIGNED> id28st_read_next_cycle;
  HWFloat<8,24> id28st_last_read_value;

  HWOffsetFix<1,0,UNSIGNED> id45out_result[3];

  HWOffsetFix<1,0,UNSIGNED> id76out_output[54];

  HWOffsetFix<1,0,UNSIGNED> id46out_result[2];

  HWOffsetFix<16,0,UNSIGNED> id48out_value;

  HWOffsetFix<16,0,UNSIGNED> id47out_value;

  HWOffsetFix<16,0,UNSIGNED> id49out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id62out_value;

  HWOffsetFix<1,0,UNSIGNED> id79out_value;

  HWOffsetFix<49,0,UNSIGNED> id59out_value;

  HWOffsetFix<48,0,UNSIGNED> id60out_count;
  HWOffsetFix<1,0,UNSIGNED> id60out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id60st_count;

  HWOffsetFix<1,0,UNSIGNED> id78out_value;

  HWOffsetFix<49,0,UNSIGNED> id65out_value;

  HWOffsetFix<48,0,UNSIGNED> id66out_count;
  HWOffsetFix<1,0,UNSIGNED> id66out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id66st_count;

  HWOffsetFix<48,0,UNSIGNED> id68out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id69out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWFloat<8,24> c_hw_flt_8_24_undef;
  const HWOffsetFix<10,0,UNSIGNED> c_hw_fix_10_0_uns_undef;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<32,0,TWOSCOMPLEMENT> c_hw_fix_32_0_sgn_undef;
  const HWFloat<8,24> c_hw_flt_8_24_bits;
  const HWOffsetFix<16,0,UNSIGNED> c_hw_fix_16_0_uns_bits;
  const HWOffsetFix<16,0,UNSIGNED> c_hw_fix_16_0_uns_bits_1;
  const HWOffsetFix<16,0,UNSIGNED> c_hw_fix_16_0_uns_undef;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* SECONDKERNEL_H_ */
