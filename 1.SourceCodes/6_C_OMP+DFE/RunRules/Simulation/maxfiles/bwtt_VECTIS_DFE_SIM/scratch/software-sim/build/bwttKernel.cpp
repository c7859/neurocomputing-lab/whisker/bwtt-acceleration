#include "stdsimheader.h"
#include "bwttKernel.h"

namespace maxcompilersim {

bwttKernel::bwttKernel(const std::string &instance_name) : 
  ManagerBlockSync(instance_name),
  KernelManagerBlockSync(instance_name, 77, 2, 0, 0, "",1)
, c_hw_fix_1_0_uns_bits((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x0l))))
, c_hw_fix_32_0_sgn_undef((HWOffsetFix<32,0,TWOSCOMPLEMENT>()))
, c_hw_fix_10_0_uns_undef((HWOffsetFix<10,0,UNSIGNED>()))
, c_hw_flt_8_24_undef((HWFloat<8,24>()))
, c_hw_fix_1_0_uns_bits_1((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x1l))))
, c_hw_fix_49_0_uns_bits((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x1000000000000l))))
, c_hw_fix_49_0_uns_bits_1((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000000l))))
, c_hw_fix_49_0_uns_bits_2((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000001l))))
{
  { // Node ID: 23 (NodeInputMappedReg)
    registerMappedRegister("io_px_force_disabled", Data(1));
  }
  { // Node ID: 3 (NodeInputMappedReg)
    registerMappedRegister("io_so_force_disabled", Data(1));
  }
  { // Node ID: 5 (NodeInput)
     m_so =  registerInput("so",1,5);
  }
  { // Node ID: 0 (NodeInputMappedReg)
    registerMappedRegister("io_si_force_disabled", Data(1));
  }
  { // Node ID: 2 (NodeInput)
     m_si =  registerInput("si",0,5);
  }
  { // Node ID: 52 (NodeMappedRom)
    registerMappedMemory("mappedRomX", 32, 1000);
  }
  { // Node ID: 54 (NodeMappedRom)
    registerMappedMemory("mappedRomRho", 32, 1000);
  }
  { // Node ID: 26 (NodeOutput)
    m_px = registerOutput("px",0 );
  }
  { // Node ID: 28 (NodeInputMappedReg)
    registerMappedRegister("io_bx_force_disabled", Data(1));
  }
  { // Node ID: 31 (NodeOutput)
    m_bx = registerOutput("bx",1 );
  }
  { // Node ID: 33 (NodeInputMappedReg)
    registerMappedRegister("io_py_force_disabled", Data(1));
  }
  { // Node ID: 53 (NodeMappedRom)
    registerMappedMemory("mappedRomY", 32, 1000);
  }
  { // Node ID: 36 (NodeOutput)
    m_py = registerOutput("py",2 );
  }
  { // Node ID: 38 (NodeInputMappedReg)
    registerMappedRegister("io_by_force_disabled", Data(1));
  }
  { // Node ID: 41 (NodeOutput)
    m_by = registerOutput("by",3 );
  }
  { // Node ID: 43 (NodeInputMappedReg)
    registerMappedRegister("io_siRho_force_disabled", Data(1));
  }
  { // Node ID: 46 (NodeOutput)
    m_siRho = registerOutput("siRho",4 );
  }
  { // Node ID: 48 (NodeInputMappedReg)
    registerMappedRegister("io_soRho_force_disabled", Data(1));
  }
  { // Node ID: 51 (NodeOutput)
    m_soRho = registerOutput("soRho",5 );
  }
  { // Node ID: 59 (NodeConstantRawBits)
    id59out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 75 (NodeConstantRawBits)
    id75out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 56 (NodeConstantRawBits)
    id56out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 60 (NodeOutputMappedReg)
    registerMappedRegister("current_run_cycle_count", Data(48), true);
  }
  { // Node ID: 74 (NodeConstantRawBits)
    id74out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 62 (NodeConstantRawBits)
    id62out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 65 (NodeInputMappedReg)
    registerMappedRegister("run_cycle_count", Data(48));
  }
}

void bwttKernel::resetComputation() {
  resetComputationAfterFlush();
}

void bwttKernel::resetComputationAfterFlush() {
  { // Node ID: 23 (NodeInputMappedReg)
    id23out_io_px_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_px_force_disabled");
  }
  { // Node ID: 3 (NodeInputMappedReg)
    id3out_io_so_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_so_force_disabled");
  }
  { // Node ID: 5 (NodeInput)

    (id5st_read_next_cycle) = (c_hw_fix_1_0_uns_bits);
    (id5st_last_read_value) = (c_hw_fix_32_0_sgn_undef);
  }
  { // Node ID: 0 (NodeInputMappedReg)
    id0out_io_si_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_si_force_disabled");
  }
  { // Node ID: 2 (NodeInput)

    (id2st_read_next_cycle) = (c_hw_fix_1_0_uns_bits);
    (id2st_last_read_value) = (c_hw_fix_32_0_sgn_undef);
  }
  { // Node ID: 67 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id67out_output[i] = (c_hw_fix_10_0_uns_undef);
    }
  }
  { // Node ID: 68 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id68out_output[i] = (c_hw_fix_10_0_uns_undef);
    }
  }
  { // Node ID: 28 (NodeInputMappedReg)
    id28out_io_bx_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_bx_force_disabled");
  }
  { // Node ID: 70 (NodeFIFO)

    for(int i=0; i<49; i++)
    {
      id70out_output[i] = (c_hw_flt_8_24_undef);
    }
  }
  { // Node ID: 69 (NodeFIFO)

    for(int i=0; i<41; i++)
    {
      id69out_output[i] = (c_hw_flt_8_24_undef);
    }
  }
  { // Node ID: 33 (NodeInputMappedReg)
    id33out_io_py_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_py_force_disabled");
  }
  { // Node ID: 38 (NodeInputMappedReg)
    id38out_io_by_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_by_force_disabled");
  }
  { // Node ID: 72 (NodeFIFO)

    for(int i=0; i<49; i++)
    {
      id72out_output[i] = (c_hw_flt_8_24_undef);
    }
  }
  { // Node ID: 43 (NodeInputMappedReg)
    id43out_io_siRho_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_siRho_force_disabled");
  }
  { // Node ID: 48 (NodeInputMappedReg)
    id48out_io_soRho_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_soRho_force_disabled");
  }
  { // Node ID: 57 (NodeCounterV1)

    (id57st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 63 (NodeCounterV1)

    (id63st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 65 (NodeInputMappedReg)
    id65out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void bwttKernel::updateState() {
  { // Node ID: 23 (NodeInputMappedReg)
    id23out_io_px_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_px_force_disabled");
  }
  { // Node ID: 3 (NodeInputMappedReg)
    id3out_io_so_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_so_force_disabled");
  }
  { // Node ID: 0 (NodeInputMappedReg)
    id0out_io_si_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_si_force_disabled");
  }
  { // Node ID: 28 (NodeInputMappedReg)
    id28out_io_bx_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_bx_force_disabled");
  }
  { // Node ID: 33 (NodeInputMappedReg)
    id33out_io_py_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_py_force_disabled");
  }
  { // Node ID: 38 (NodeInputMappedReg)
    id38out_io_by_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_by_force_disabled");
  }
  { // Node ID: 43 (NodeInputMappedReg)
    id43out_io_siRho_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_siRho_force_disabled");
  }
  { // Node ID: 48 (NodeInputMappedReg)
    id48out_io_soRho_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_soRho_force_disabled");
  }
  { // Node ID: 65 (NodeInputMappedReg)
    id65out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void bwttKernel::preExecute() {
  { // Node ID: 5 (NodeInput)
    if(((needsToReadInput(m_so))&(((getFlushLevel())<((4l)+(5)))|(!(isFlushingActive()))))) {
      (id5st_last_read_value) = (readInput<HWOffsetFix<32,0,TWOSCOMPLEMENT> >(m_so));
    }
    id5out_data = (id5st_last_read_value);
  }
  { // Node ID: 2 (NodeInput)
    if(((needsToReadInput(m_si))&(((getFlushLevel())<((4l)+(5)))|(!(isFlushingActive()))))) {
      (id2st_last_read_value) = (readInput<HWOffsetFix<32,0,TWOSCOMPLEMENT> >(m_si));
    }
    id2out_data = (id2st_last_read_value);
  }
}

void bwttKernel::runComputationCycle() {
  if (m_mappedElementsChanged) {
    m_mappedElementsChanged = false;
    updateState();
    std::cout << "bwttKernel: Mapped Elements Changed: Reloaded" << std::endl;
  }
  preExecute();
  execute0();
}

int bwttKernel::getFlushLevelStart() {
  return ((1l)+(3l));
}

}
