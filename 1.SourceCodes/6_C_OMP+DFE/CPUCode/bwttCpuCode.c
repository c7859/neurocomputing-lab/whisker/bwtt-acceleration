#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "sdGeneric.h"
#include "stShapeSpaceKalman.h"
#include "ppBgExtractionAndFilter.h"
#include "wdIgorMeanAngle.h"


#include "/home/yangma/workspace/BWTT_MAIA/RunRules/DFE/include/Maxfiles.h"
#include "MaxSLiCInterface.h"

int main(void)
{

	//-----------------------Main Parameter Setting--------------------
		// Segment of frame for processing
		int startFrame = 8000, endFrame = 8002;

		// Parameters
		int snoutCenterX = 389;
		int snoutCenterY = 145;
		int snoutTipX = 417;
		int snoutTipY = 328;

		FILE * fp;
		fp = fopen("parameters", "r");

		fscanf(fp, "Starting Frame Index:%d\n", &startFrame);
		fscanf(fp, "End Frame Index:%d\n", &endFrame);
		fscanf(fp, "Snout Center X:%d\n", &snoutCenterX);
		fscanf(fp, "Snout Center Y:%d\n", &snoutCenterY);
		fscanf(fp, "Snout Tip X:%d\n", &snoutTipX);
		fscanf(fp, "Snout Tip Y:%d", &snoutTipY);
		fclose(fp);

		/*
		printf("Please enter the index of starting frame: \n");
		scanf("%d", &startFrame);
		printf("Please enter the index of end frame: \n");
		scanf("%d", &endFrame);
		printf("Please enter the X of snout center: \n");
		scanf("%d", &snoutCenterX);
		printf("Please enter the Y of snout center: \n");
		scanf("%d", &snoutCenterY);
		printf("Please enter the X of snout tip: \n");
		scanf("%d", &snoutTipX);
		printf("Please enter the Y of snout tip \n");
		scanf("%d", &snoutTipY);
		*/

		clock_t start = clock(), diff;

		// Read Video File
		CvCapture* capture = cvCaptureFromFile("150924-132427-003-00000-00000.avi");
		//--------------------------The End ---------------------------------

		uchar *frameData;
		IplImage *frame = 0;
		int boundaryX[10000];
		int boundaryY[10000];

		FILE *file1;
		file1 = fopen("source", "w");



		//cvNamedWindow("Capture", CV_WINDOW_AUTOSIZE);

		//cvShowImage("Capture", frame); // Display the frame

		/*
		for (int i = 0; i < nRows; i++)
				for (int j = 0; j < nCols; j++)
					if (frameData[i*nCols + j] > ppBg[i*nCols + j]) {
						ppBg[i*nCols + j] = frameData[i*nCols + j];
					}
		*/

		int frameNumbers = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_COUNT);

		if (endFrame > frameNumbers) {
			endFrame = frameNumbers;
		}

		int nRows = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_HEIGHT);
		int nCols = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_WIDTH);
		//int step = 2;
		int step = (frameNumbers - startFrame) / 1000;
		int pixs = nRows*nCols;

		IplImage *greyframe = cvCreateImage(
			cvSize(nCols, nRows), IPL_DEPTH_8U, 1);



		uchar *ppBg;

		ppBg = (uchar*)calloc(nRows*nCols, sizeof(uchar));
		memset(ppBg, 0, nRows*nCols);

		// Initialize background image
		printf("Extracting Background Image...\n");
		for (int index = startFrame; index < frameNumbers; index += step) {
			cvSetCaptureProperty(capture, CV_CAP_PROP_POS_FRAMES, index);
			frame = cvQueryFrame(capture);

			cvCvtColor(frame, greyframe, CV_BGR2GRAY);
			frameData = (uchar *)greyframe->imageData;
			for (int i = 0; i < pixs; i++) {
				if (frameData[i] > ppBg[i]) {
					ppBg[i] = frameData[i];
				}
			}
		}

		cvSetCaptureProperty(capture, CV_CAP_PROP_POS_FRAMES, startFrame);
		frame = cvQueryFrame(capture);
		cvCvtColor(frame, greyframe, CV_BGR2GRAY);

		// Calculate Generic Boundaries
		int boundaryCount = sdGeneric(greyframe, boundaryX, boundaryY);

		// Create Snout Ring
		IplImage *RingMask = cvCreateImage(
			cvSize(nCols, nRows), IPL_DEPTH_8U, 1);


		IplImage *distanceMask = cvCreateImage(
			cvSize(nCols, nRows), IPL_DEPTH_32F, 1);
		cvSetZero(RingMask);

		initializeFromParametersSnoutTemplate(snoutCenterX, snoutCenterY,
			snoutTipX, snoutTipY, nRows, nCols,
			boundaryX, boundaryY, boundaryCount, RingMask, distanceMask);

		// Iterate through all video frames
		cvSetCaptureProperty(capture, CV_CAP_PROP_POS_FRAMES, startFrame);

		for (int i = startFrame; i < endFrame; i++) {

			printf("Processing Frame %d\n", i);
			frame = cvQueryFrame(capture);
			cvCvtColor(frame, greyframe, CV_BGR2GRAY);

			// Extract Whisker Images
			ppExtractionAndFilter(greyframe, pixs, ppBg);

			fprintf(file1, "---\n");
			// Calculate Mean Angle
			wdIgorMeanAngleProcess(greyframe, RingMask, nCols, nRows, pixs, 389, 145,
				distanceMask, file1);
		}



		// wait for a key
		//cvWaitKey(0);

		// release the image
		cvReleaseImage(&greyframe);
		cvReleaseImage(&RingMask);
		cvReleaseCapture(&capture);
		cvReleaseImage(&distanceMask);


		free(ppBg);
		fclose(file1);

		diff = clock() - start;
		int msec = diff * 1000 / CLOCKS_PER_SEC;
		printf("Processed 10000 frames \n");
		printf("Time taken %d seconds %d milliseconds\n", msec/1000, msec%1000);



		/*


	// The Maxeler Example Code Legacy!!!!
	const int size = 384;
	int sizeBytes = size * sizeof(int32_t);
	int32_t *x = malloc(sizeBytes);
	int32_t *y = malloc(sizeBytes);
	int32_t *s = malloc(sizeBytes);

	// TODO Generate input data
	for(int i = 0; i < size; ++i) {
		x[i] = random() % 100;
		y[i] = random() % 100;
	}

	printf("Running on DFE.\n");
	int scalar = 3;
	bwtt(scalar, size, x, y, s);

	// TODO Use result data
	for(int i = 0; i < size; ++i)
		if ( s[i] != x[i] + y[i] + scalar)
			return 1;

	*/

	printf("Done.\n");


	return 0;
}
