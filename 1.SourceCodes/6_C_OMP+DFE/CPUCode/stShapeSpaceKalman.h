/**
* @file sdSpaceKalman.h
* @Funtions for Calculating Snout Data
* @author Yang Ma, Erasmus MC
*/

/*
% This function attempts to find the snout contour using a pre-defined contour template
% Inputs:
%           -(snoutCenterX, snoutCenterY) is the user selected snout center
%           -(noseTipX, noseTipY) is the user selected nose tip
%           -sdContour is a vector of points extracted from previous section
% Outputs:
%           -snoutContour is the estiamted contour of snout
*/

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void initializeFromParametersSnoutTemplate(int snoutCenterX, int snoutCenterY, int noseTipX, int noseTipY,
	int rows, int cols, int *boundaryX, int *boundaryY, int boundaryCount, IplImage *RingMask, IplImage *distanceMask);
void createRingMask(int rows, int cols, int *snoutX, int *snoutY, int snoutCount, IplImage *RingMask, IplImage *distanceMask);
void createDiskStrel(int x, uchar * disk);

//X coordinate of snout template
float ppTemplateX[300] = { 106.61755, 106.45193, 106.27908, 106.09176, 105.88274, 105.64478, 105.37070,
105.05578, 104.69925, 104.30073, 103.85982, 103.37612, 102.84992, 102.28510, 101.68678, 101.06003, 100.40999,
99.742790, 99.075836, 98.433273, 97.839371, 97.318375, 96.893059, 96.549149, 96.233582, 95.891457, 95.467903,
94.908028, 94.181953, 93.342781, 92.460785, 91.606262, 90.849495, 90.236458, 89.714012, 89.203789, 88.627441,
87.906601, 86.978027, 85.895569, 84.767822, 83.703667, 82.811996, 82.185493, 81.783142, 81.498055, 81.222900,
80.850342, 80.274254, 79.455414, 78.454811, 77.341698, 76.185287, 75.054596, 74.004265, 73.066315, 72.270775,
71.647675, 71.226700, 70.999702, 70.886711, 70.799751, 70.650848, 70.352013, 69.821121, 69.071381, 68.193626,
67.280952, 66.426445, 65.720146, 65.185226, 64.779709, 64.458923, 64.178192, 63.892864, 63.569054, 63.205902,
62.808842, 62.383301, 61.934711, 61.469379, 61.003696, 60.560516, 60.162792, 59.833469, 59.595387, 59.456696,
59.397026, 59.392742, 59.420193, 59.455746, 59.475750, 59.456684, 59.375210, 59.208023, 58.931808, 58.523258,
57.961220, 57.256634, 56.445057, 55.562675, 54.645672, 53.729683, 52.839420, 51.989586, 51.194500, 50.468483,
49.824902, 49.250759, 48.704151, 48.141693, 47.520000, 46.795856, 45.945923, 44.984867, 43.931641, 42.805199,
41.624493, 40.409576, 39.182678, 37.966293, 36.782913, 35.655003, 34.593239, 33.579529, 32.591480, 31.606688,
30.602760, 29.557919, 28.451971, 27.264982, 25.977018, 24.568150, 23.038063, 21.448162, 19.871964, 18.382980,
17.054726, 15.953522, 15.060153, 14.299281, 13.594593, 12.869775, 12.052078, 11.123896, 10.110979, 9.0402489,
7.9386282, 6.8313966, 5.7164407, 4.5689979, 3.3636243, 2.0748775, 0.67846841, -0.82387835, -2.4047165,
-4.0355043, -5.6876988, -7.3327589, -8.9439383, -10.506771, -12.011928, -13.450093, -14.811956, -16.088148,
-17.267193, -18.334911, -19.276945, -20.078939, -20.727407, -21.245869, -21.707802, -22.190222, -22.770144,
-23.524454, -24.493338, -25.629501, -26.872896, -28.163477, -29.441212, -30.663836, -31.840628, -32.990162,
-34.131020, -35.281776, -36.454857, -37.640850, -38.825500, -39.994541, -41.133717, -42.229942, -43.275257,
-44.263119, -45.186962, -46.040241, -46.822475, -47.561256, -48.292282, -49.051247, -49.873848, -50.787331,
-51.770836, -52.786533, -53.796566, -54.763081, -55.653988, -56.471970, -57.232697, -57.951847, -58.645115,
-59.324768, -59.977505, -60.578430, -61.102585, -61.525024, -61.823067, -62.009926, -62.127392, -62.218033,
-62.324429, -62.489159, -62.745468, -63.077297, -63.452339, -63.838284, -64.202805, -64.514351, -64.759842,
-64.945320, -65.077690, -65.163864, -65.210770, -65.228180, -65.246651, -65.305847, -65.445450, -65.705162,
-66.124657, -66.722420, -67.458893, -68.284676, -69.150345, -70.006508, -70.816872, -71.589111, -72.340118,
-73.086769, -73.845955, -74.628876, -75.423416, -76.211456, -76.974861, -77.695526, -78.358444, -78.964493,
-79.519516, -80.029381, -80.499939, -80.939270, -81.377258, -81.856216, -82.418587, -83.106827, -83.960686,
-84.959244, -86.021667, -87.064545, -88.004478, -88.761330, -89.335335, -89.810196, -90.273499, -90.812813,
-91.515144, -92.411499, -93.431908, -94.495773, -95.522507, -96.431847, -97.179474, -97.789444, -98.293465,
-98.723259, -99.110527, -99.480873, -99.838844, -100.18440, -100.51755, -100.83825, -101.14707, -101.45220,
-101.76732, -102.10620, -102.48261, -102.91032, -103.39562, -103.92871, -104.49763, -105.09045, -105.69522 };

//Y coordinate of snout template
float ppTemplateY[300] = { -104.81802, -103.21561, -101.61725, -100.02699, -98.448891, -96.887001, -95.345367,
-93.826988, -92.333214, -90.865273, -89.424355, -88.011673, -86.627876, -85.270546, -83.936241, -82.621521,
-81.322937, -80.036316, -78.749619, -77.446068, -76.108810, -74.721008, -73.266869, -71.756905, -70.229172,
-68.723007, -67.277756, -65.932770, -64.708931, -63.565853, -62.450478, -61.309734, -60.090546, -58.758904,
-57.358395, -55.952370, -54.604179, -53.377174, -52.322441, -51.396042, -50.509628, -49.574604, -48.502373,
-47.217472, -45.752808, -44.194656, -42.629673, -41.144512, -39.824810, -38.700840, -37.719887, -36.822426,
-35.948925, -35.040127, -34.053471, -32.972721, -31.783957, -30.473261, -29.026928, -27.454475, -25.809504,
-24.150537, -22.536104, -21.024725, -19.670961, -18.464748, -17.343405, -16.242722, -15.098487, -13.848616,
-12.477638, -11.015489, -9.4939785, -7.9449205, -6.4001222, -4.8834724, -3.3946362, -1.9286604, -0.48059285,
0.95451903, 2.3819809, 3.8111579, 5.2540102, 6.7225409, 8.2287521, 9.7845860, 11.394504, 13.048441, 14.734662,
16.441431, 18.157011, 19.869646, 21.565435, 23.226658, 24.835205, 26.372961, 27.821815, 29.165201, 30.409586,
31.579103, 32.698334, 33.791866, 34.883972, 35.992794, 37.130856, 38.310471, 39.543953, 40.842831, 42.197102,
43.573143, 44.936127, 46.251221, 47.483723, 48.613312, 49.647186, 50.595654, 51.469013, 52.277603, 53.037212,
53.774609, 54.517868, 55.295078, 56.134281, 57.049881, 58.022980, 59.029701, 60.046158, 61.048473, 62.010738,
62.901871, 63.689957, 64.343079, 64.829338, 65.143562, 65.364670, 65.588097, 65.909271, 66.423615, 67.217598,
68.271263, 69.494797, 70.797180, 72.087372, 73.278557, 74.348671, 75.326607, 76.242645, 77.127037, 78.008072,
78.880928, 79.713379, 80.472404, 81.124977, 81.639069, 82.005417, 82.237236, 82.348717, 82.354042, 82.267395,
82.100700, 81.850441, 81.506653, 81.059326, 80.498466, 79.814209, 79.001671, 78.062332, 76.998116, 75.810928,
74.503265, 73.101974, 71.666794, 70.259804, 68.943069, 67.778572, 66.797844, 65.959946, 65.213356, 64.506561,
63.788040, 63.021732, 62.216316, 61.388554, 60.555206, 59.733040, 58.932762, 58.143620, 57.350086, 56.536633,
55.687737, 54.789845, 53.838013, 52.829651, 51.762165, 50.632965, 49.444199, 48.219933, 46.990551, 45.786446,
44.638008, 43.568356, 42.559284, 41.577991, 40.591660, 39.567467, 38.477566, 37.324097, 36.120384, 34.879776,
33.615623, 32.339016, 31.044188, 29.717716, 28.346148, 26.916025, 25.415152, 23.851091, 22.247156, 20.627090,
19.014639, 17.433546, 15.902435, 14.412847, 12.947402, 11.488713, 10.019397, 8.5224543, 6.9903579, 5.4253683,
3.8301983, 2.2075601, 0.56016660, -1.1072749, -2.7756622, -4.4195757, -6.0135694, -7.5321965, -8.9500303,
-10.255132, -11.472539, -12.633568, -13.769533, -14.911752, -16.083170, -17.278692, -18.487352, -19.698187,
-20.900230, -22.086330, -23.264997, -24.448774, -25.650202, -26.881823, -28.154118, -29.467163, -30.817757,
-32.202686, -33.618748, -35.060856, -36.505363, -37.918083, -39.264702, -40.510899, -41.624527, -42.622169,
-43.568562, -44.530483, -45.574722, -46.765442, -48.102425, -49.518597, -50.943764, -52.307743, -53.540829,
-54.618351, -55.596954, -56.541824, -57.518135, -58.590801, -59.794842, -61.108429, -62.503365, -63.951462,
-65.424522, -66.900200, -68.376358, -69.855209, -71.338982, -72.829895, -74.329697, -75.833885, -77.333519,
-78.819542, -80.282898, -81.714569, -83.110298, -84.476166, -85.819649, -87.148209, -88.469299 };

void initializeFromParametersSnoutTemplate(int snoutCenterX, int snoutCenterY, int noseTipX, int noseTipY,
	int rows, int cols, int *boundaryX, int *boundaryY, int boundaryCount, IplImage *RingMask, IplImage *distanceMask) {

	float tcos = noseTipY - snoutCenterY;
	float tsin = noseTipX - snoutCenterX;

	float r = sqrtf(tcos*tcos + tsin*tsin);

	// The furthest point distance on the template
	float preBound = 3.51 * r * r;
	//float preBound = 1.8689 * r;

	int snoutX[300];
	int snoutY[300];
	int snoutCount = 0;


	int preselectCount = 0;
	for (int i = 0; i < boundaryCount; i++) {
		int tempX = boundaryX[i] - snoutCenterX;
		int tempY = boundaryY[i] - snoutCenterY;
		if (tempX*tempX + tempY*tempY < preBound) {
			int temp = boundaryX[i];
			boundaryX[i] = boundaryX[preselectCount];
			boundaryX[preselectCount] = temp;
			temp = boundaryY[i];
			boundaryY[i] = boundaryY[preselectCount];
			boundaryY[preselectCount] = temp;
			preselectCount++;
		}
	}

	// Adjust the template based on user selected points
	for (int i = 0; i < 300; i++) {
		float tempX = (ppTemplateY[i] * tsin + ppTemplateX[i] * tcos) / 80 + snoutCenterX;
		float tempY = (ppTemplateY[i] * tcos - ppTemplateX[i] * tsin) / 80 + snoutCenterY;

		// Rule out points of template that are out of frame
		if (tempX < 0 || tempY < 0 || tempX > cols || tempY > rows) {
			continue;
		}

		// Find closet fit template <--> environment contour
		// Didn't implement finding closer fit on original code
		float tempMin = FLT_MAX;
		int tempMinIndex = -1;
		for (int j = 0; j < preselectCount; j++) {
			float disTempX = tempX - boundaryX[j];
			float disTempY = tempY - boundaryY[j];
			if (disTempX*disTempX + disTempY*disTempY < tempMin) {
				tempMin = disTempX*disTempX + disTempY*disTempY;
				tempMinIndex = j;
			}
		}
		snoutX[snoutCount] = boundaryX[tempMinIndex];
		snoutY[snoutCount] = boundaryY[tempMinIndex];
		snoutCount++;
	}

	createRingMask(rows, cols, snoutX, snoutY, snoutCount, RingMask, distanceMask);

}

void createRingMask(int rows, int cols, int *snoutX, int *snoutY, int snoutCount,
	IplImage *RingMask, IplImage *distanceMask) {

	int pixs = cols*rows;
	// minD in wdParameters
	int distanceFromSnout = 20;
	// bandwidth in wdParameters
	int bandWidth = 8;

	int internalBand = distanceFromSnout + bandWidth;

	/*
	uchar outDisk[10000] = { 0 };
	uchar innerDisk[10000] = { 0 };
	createDiskStrel(internalBand, outDisk);
	createDiskStrel(distanceFromSnout, innerDisk);
	IplConvKernel *selOuter = cvCreateStructuringElementEx(internalBand * 2 - 1,
	internalBand * 2 - 1, 0, 0, CV_SHAPE_CUSTOM, outDisk);
	IplConvKernel *selInner = cvCreateStructuringElementEx(distanceFromSnout * 2 - 1,
	distanceFromSnout * 2 - 1, 0, 0, CV_SHAPE_CUSTOM, innerDisk);
	*/



	IplConvKernel *selOuter = cvCreateStructuringElementEx(internalBand * 2 - 1,
		internalBand * 2 - 1, 0, 0, CV_SHAPE_ELLIPSE, NULL);
	IplConvKernel *selInner = cvCreateStructuringElementEx(distanceFromSnout * 2 - 1,
		distanceFromSnout * 2 - 1, 0, 0, CV_SHAPE_ELLIPSE, NULL);



	IplImage *BigRing = cvCreateImage(
		cvSize(cols, rows), IPL_DEPTH_8U, 1);
	IplImage *SmallRing = cvCreateImage(
		cvSize(cols, rows), IPL_DEPTH_8U, 1);

	uchar *maskData = (uchar *)RingMask->imageData;
	for (int i = 0; i < snoutCount; i++) {
		maskData[snoutX[i] + snoutY[i] * cols] = 1;
	}


	cvDilate(RingMask, BigRing, selOuter, 1);
	cvDilate(RingMask, SmallRing, selInner, 1);



	uchar *bigData = (uchar *)BigRing->imageData;
	uchar *smallData = (uchar *)SmallRing->imageData;

	cvDistTransform(BigRing, distanceMask, CV_DIST_L2, 5, NULL, NULL, CV_DIST_LABEL_CCOMP);

	for (int i = 0; i < pixs; i++) {
		int temp = bigData[i] - smallData[i];
		if (temp < 0) {
			temp = 0;
		}
		maskData[i] = temp;
		bigData[i] = 1 - bigData[i];
	}


	cvReleaseImage(&BigRing);
	cvReleaseImage(&SmallRing);
}

void createDiskStrel(int x, uchar * disk) {
	int width = 2 * x - 1;
	int whiteSize = x / 2 + 1;
	int temp = whiteSize;
	for (int i = 0; i < whiteSize; i++) {
		for (int j = temp; j < width - temp; j++) {
			disk[i * width + j] = 1;
		}
		temp--;
	}

	for (int i = whiteSize; i < width - whiteSize; i++) {
		for (int j = 0; j < width; j++) {
			disk[i * width + j] = 1;
		}
	}

	temp = 1;
	for (int i = width - whiteSize; i < width; i++) {
		for (int j = temp; j < width - temp; j++) {
			disk[i * width + j] = 1;
		}
		temp++;
	}
}
