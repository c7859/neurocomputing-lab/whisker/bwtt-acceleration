All 6 implementations were included in this folder, including 2 Matlab-based optimizations on BWTT and 4 C-based implementations.

In the folder of each implementation, there is an additional readme file indicating the basic changes or optimization for each implementation.