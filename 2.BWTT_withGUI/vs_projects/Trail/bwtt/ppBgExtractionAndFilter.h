/**
* @file ppBgExtractionAndFilter.h
* @Funtions for Processing Grey Image of Whiskers
* @author Yang Ma, Erasmus MC
*/

/*
% This function attempts to high-light and pop out whiskers on the image
% Inputs:
%           -greyFrame is the grey image of a video frame
%           -pixs is the total amount of pixels
%           -ppBg is pre-extracted background image
*/

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

uchar adjustLUT[256] = { 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 3, 4, 5, 6, 7, 8,
9, 11, 12, 14, 16, 18, 20, 22, 24, 27, 29, 32, 35, 38, 42, 45, 49,
52, 56, 61, 65, 69, 74, 79, 84, 89, 94, 100, 106, 112, 118, 124, 131,
138, 145, 152, 159, 167, 175, 183, 191, 199, 208, 217, 226, 236, 245,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255 };


void ppExtractionAndFilter(IplImage *greyFrame, int pixs, uchar *ppBg, uchar *bwShapeData);
void imadjust(uchar *frameData, int pixs);

void ppExtractionAndFilter(IplImage *greyFrame, int pixs, uchar *ppBg, uchar *bwShapeData) {

	// Parameter Setting
	//double bwThreshold = 0.2;
	//double histEqGamma = 2.5;
	//int histEqMinT = 0;
	//int histEqMaxT = 64;
	//int razorSize = 15;

	/*
	uchar razorDisk[1000] = {0};
	createDiskStrel(razorSize, razorDisk);
	IplConvKernel *erodeDisk = cvCreateStructuringElementEx(razorSize * 2 - 1,
	razorSize * 2 - 1, 0, 0, CV_SHAPE_CUSTOM, razorDisk);
	*/

	//IplConvKernel *erodeDisk = cvCreateStructuringElementEx(razorSize * 2 - 1,
	//razorSize * 2 - 1, 0, 0, CV_SHAPE_ELLIPSE, NULL);

	//IplImage *bwShape = cvCloneImage(greyFrame);
	//stExtractEnvGrayValuesWithMorphology(bwShape, bwThreshold, erodeDisk);

	uchar *frameData;
	frameData = (uchar *)greyFrame->imageData;

	//uchar *bwShapeData;
	//bwShapeData = (uchar *)bwShape->imageData;

	// Background Extraction
	for (int i = 0; i < pixs; i++) {
		//frameData[i] = (255 - frameData[i])*bwShapeData[i];
		frameData[i] = (ppBg[i] - frameData[i])*bwShapeData[i];
	}



	imadjust(frameData, pixs);

}


void imadjust(uchar *frameData, int pixs)
{
	// src : input CV_8UC1 image
	// highIn  : src image bounds

	for (int i = 0; i < pixs; i++) {
		/*
		if (frameData[i] > highIn) {
		frameData[i] = highIn;
		}
		double temp = 255 * pow(((double)frameData[i] / highIn), gamma) + 0.5;
		*/

		//frameData[i] = (int)temp;
		frameData[i] = adjustLUT[frameData[i]];
	}

}
