# BWTT Acceleration

This repository contains accelerated implementation of the BWTT tool, as implemented by Yang Ma.

This package contains two parts:

I. The source codes of all implementations in this project, containing:

	1) Matlab-based data-level parallelization
	2) Matlab-based GPU-accelerated data-level parallelization
	3) C-based GPU-accelerated version
	4) C-based DFE-accelerated version
	5) OMP-accelerated version
	6) OMP+DFE-accelerated version

2. The Visual-Studiio projects for creating the GUI, and that folder contains:

	1) The final Windows software of BWTT
	2) Seperate Visual-Studio projects with different functionalities for the software.

A published paper describing the accelerated BWTT tools can be found here: [IEEE SAMOS 2017](https://ieeexplore.ieee.org/document/8344621).
